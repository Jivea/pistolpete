/**
 * Created by Jivea
 * utils.js
 * Just some utilities
 */


/* global define */
define(function(require, exports, module) {
    "use strict";

    exports.areArraysEqual = function(a, b) {
        return JSON.stringify(a) == JSON.stringify(b);
    };

    exports.trunkDateTimeToDate = function(dateTime) {
        dateTime.setHours(0, 0, 0, 0);
        return dateTime;
    };

    exports.generateGuid = function() {
            function _p8(s) {
                var p = (Math.random().toString(16)+"000000000").substr(2,8);
                return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
            }
            return _p8() + _p8(true) + _p8(true) + _p8();
    };
});