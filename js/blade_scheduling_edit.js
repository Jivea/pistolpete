/**
 * Created by Jivea
 * blade_schedule_edit.js
 * +
 */

/* global define */
define(["jquery", "moment", "app", "home", "formatter", "blading/blade", "models/scheduling", "bookingProxyX", "schedulingProxyX", "utilities"],
    function($, moment, _app, _home, formatter, Blade, Scheduling, _bookingProxy, _schedulingProxy, utilities) {
        "use strict";

        function BladeSchedulingEdit(bladeConfiguration, template, isNew) {
            var self = this;

            this._bookings = null;
            this._originalSchedule = null;
            this._editedSchedule = null;
            this._originalDay = null;

            BladeEdit.call(this, bladeConfiguration, template, isNew);

            this.loadBookings = function(done) {
                updateSpinner();

                _bookingProxy.getList(function(items) {
                    updateSpinner();

                    if(done != null)
                        done(items);
                }, function(error) {
                    _app.onError(error);
                });
            };

            this.setLimitDateForRepetitionOfSelectedBooking = function() {
                var bookingEndDate = getSelectedBooking().endDate;
                $("#scheduleForm_repetitionLimitDate").datepicker("setDate", bookingEndDate);
            };

            this.registerDOMEvents = function() {
                $("#scheduleForm_movie").on("change", onUserInput);
                $("#scheduleForm_screen").on("change", onUserInput);
                $("#scheduleForm_date").on("change paste keyup", onUserInput);
                $("#scheduleForm_time").on("change paste keyup", onUserInput);
            };

            function onUserInput() {
                self.onEditedItemChanged.call(self);
            }

            this.save = function() {
                if(this._originalSchedule) {
                    this.updateCommandButtonState(["save", "cancel"], false);

                    var editedSchedule = self.getEditedItem();
                    _schedulingProxy.saveSchedules([editedSchedule], function(savedSchedule) {
                        _app.onSuccess("Séance modifiée.");
                        _app.events.publish("scheduling-updated", savedSchedule);
                    }, function(error) {
                        _app.onError(error);
                    });
                } else {
                    this.updateCommandButtonState(["save", "cancel"], false);

                    var toSave = getSchedulesToSave();
                    _schedulingProxy.saveSchedules(toSave, function(firstScheduleSaved) {
                        _app.onSuccess("Séance(s) ajoutée(s)");
                        _app.events.publish("scheduling-added", firstScheduleSaved);
                    }, function(error) {
                        _app.onError(error);
                    });
                }
            };

            this.displayRepetitionCount = function() {
                var schedulingList = self.getSchedulesToSave();
                $("#scheduleForm_schedulingCount").text(schedulingList.length);
            };

            function getSchedulesToSave() {
                var isRepetitionEnabled = $("#scheduleForm_repetitionCbx").prop("checked");
                if(!isRepetitionEnabled) {
                    return [Scheduling.createCopy(self._editedSchedule)];
                }

                var result = [];
                var repetitionFrequency = $("#scheduleForm_repetitionFreq").find('option:selected').val();
                var endDate = moment($("#scheduleForm_repetitionLimitDate").datepicker('getDate')).endOf("day");

                var iteratorDate = moment(self._editedSchedule.scheduleTime);
                while (iteratorDate.isBefore(endDate)) {
                    var repetition = Scheduling.createCopy(self._editedSchedule);
                    repetition.scheduleTime = moment(iteratorDate);
                    result.push(repetition);

                    if(repetitionFrequency == "week") {
                        iteratorDate.add(7, "d");
                    } else {
                        iteratorDate.add(1, "d");
                    }
                }
                return result;
            }

            function getSelectedBooking() {
                var selectedBookingId = $("#scheduleForm_movie").find('option:selected').val();
                return self._bookings.getObjectWithId(selectedBookingId);
            }

            function updateSpinner() {
                Blade.prototype.showSpinner.call(self._bookings == null);
            }

            function getDatePickerConfig() {
                return {
                    changeMonth: true,
                    changeYear: true,
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    showButtonPanel: true,
                    showWeek: true,
                    firstDay: 1,
                    minDate: null,
                    dateFormat: 'dd/mm/yy'
                };
            }
        }

        BladeSchedulingEdit.prototype = Object.create(BladeEdit.prototype);
        BladeSchedulingEdit.prototype.constructor = BladeSchedulingEdit;
        BladeSchedulingEdit.prototype.getModelType = function() {
            return Scheduling;
        };
        BladeSchedulingEdit.prototype.getModelProxy = function() {
            return _schedulingProxy;
        };
        BladeSchedulingEdit.prototype.init = function(dataContext) {
            var self = this;
            this._originalDay = dataContext.currentDay;

            this.loadBookings(function(loadedItems) {
                self._bookings = loadedItems;
                dataContext.bookings = loadedItems;

                BladeEdit.prototype.init.call(self, dataContext);
            });
        };

        BladeSchedulingEdit.prototype.bindForm = function() {
            var self = this;

            var pickerDate = $("#scheduleForm_date");
            pickerDate.datepicker(getDatePickerConfig());
            var pickerTime = $("#scheduleForm_time");

            if(self._originalSchedule != null) {
                $('#scheduleForm_movie option[value="' + self._originalSchedule.bookingId + '"]').prop("selected", true);
                $('#scheduleForm_screen option[value="' + self._originalSchedule.screenId + '"]').prop("selected", true);
                pickerDate.datepicker("setDate", self._originalSchedule.scheduleTime.toDate());
                pickerTime.val(formatter.formatMomentToLT(self._originalSchedule.scheduleTime));
            } else {
                pickerDate.datepicker("setDate", self._originalDay.toDate());
                pickerTime.val(formatter.formatMomentToLT(moment().hours(20).minutes(0)));

                var pickerLimitDate = $("#scheduleForm_repetitionLimitDate");
                pickerLimitDate.datepicker(getDatePickerConfig());
                self.setLimitDateForRepetitionOfSelectedBooking();
            }

            if(self._originalSchedule) { // do not permit multi add when editing
                $(".scheduleForm_repetitionGroup").addClass("hidden");
            } else {
                $("#scheduleForm_repetitionCbx").on("click", function() {
                    $(".scheduleForm_inRepeat").toggleClass("hidden");
                    self.displayRepetitionCount();
                });
            }
        };

        BladeSchedulingEdit.prototype.getEditedItem = function() {
            var self = this;

            self._editedSchedule.id = self._originalSchedule ? self._originalSchedule.id : "";
            self._editedSchedule.bookingId = $("#scheduleForm_movie").val();
            self._editedSchedule.movieTitle = $('#scheduleForm_movie').children("option:selected").text();
            self._editedSchedule.screenId = $("#scheduleForm_screen").val();

            var scheduleDate = $("#scheduleForm_date").datepicker('getDate');
            var scheduleTime = moment($("#scheduleForm_time").val(), "HH:mm");
            self._editedSchedule.scheduleTime = moment.utc()
                .year(scheduleDate.getFullYear())
                .month(scheduleDate.getMonth())
                .date(scheduleDate.getDate())
                .hours(scheduleTime.hours())
                .minutes(scheduleTime.minutes())
                .seconds(0)
                .milliseconds(0);

            return self._editedSchedule;
        };

        BladeBookingEdit.prototype.subscribeDOMEvents = function() {
            this.registerDOMEvents();
        };

        return BladeSchedulingEdit;
    });