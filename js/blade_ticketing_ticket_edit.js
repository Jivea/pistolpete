/**
 * Created by Jivea
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var Ticket = require("models/ticket");

    var $ = require("jquery");
    var _app = require("app");
    var _home = require("home");
    var _proxy = require("ticketingProxyX");
    var _bladeDefinitions = require("blades/bladeDefinitions");
    var _utilities = require("utilities");
    var _bladeCommon = require("blade_common");

    var _isNew;
    var _bladeName;
    var _original,
        _editedItem;


    exports.init = function(dataContext) {
        _isNew = dataContext == null;
        _original = dataContext;
        _bladeName = _isNew ? "ticketing_configTicket_new" : "ticketing_configTicket_edit";

        _bladeCommon.init(_bladeName, _isNew ? null : _original.Name);

        initTicketEdition();

        _app.events.subscribe("blade-closed", onBladeClosed);
        _app.events.subscribe("ticketing_configTicket_trad-updated", onUserInput);

    };
    exports.unInit = function() {
        _app.events.unSubscribe("blade-closed", onBladeClosed);
        _app.events.unSubscribe("ticketing_configTicket_trad-updated", onUserInput);
    };

    exports.onCommandSave = function() {
        updateCommandButtonsState(false);

        var editedItem = getEditedTicket();
        _proxy.saveTicket(editedItem, function(savedItem) {

            if(_isNew) {
                _app.onSuccess("Nouveau ticket ajouté.");
                _app.events.publish("ticketing_configTicket-saved", savedItem);
            } else {
                _app.onSuccess("Ticket modifié.");
                _app.events.publish("ticketing_configTicket-updated", savedItem);
            }
        }, function(error) {
            _app.onError(error);
        });
    };

    exports.onCommandDelete = function() {
        _proxy.deleteTicket(_original, function() {
            _app.onSuccess("Ticket successfully deleted");
            _app.events.publish("ticketing_configTicket-deleted");
        }, function(error) {
            _app.onError(error);
        });
    };

    exports.onCommandCancel = function() {
        _home.closeBlade("ticketing_configTicket_trad", function() {
            initTicketEdition();
        });

    };

    function initTicketEdition() {
        _editedItem = _isNew ? new Ticket() : Ticket.createCopy(_original);
        loadForm();
        updateCommandButtonsState(false);
        bindForm(_editedItem);
    }

    function loadForm() {
        var container = $("#blade-content-" + _bladeName);
        var form = require("hbs!templates/ticketForm")();

        container.empty();
        container.append(form);
    }

    function bindForm(item) {

        var txtName = $("#ticketForm_name");
        var txtPrice = $("#ticketForm_price");
        var txtVat = $("#ticketForm_vat");
        var txtConditions = $("#ticketForm_conditions");

        if(item != null) {
            txtName.val(item.name);
            txtPrice.val(item.price);
            txtVat.val(item.vat);
            txtConditions.val(item.condition);
        }

        txtName.on("change paste keyup", onUserInput);
        txtPrice.on("change paste keyup", onUserInput);
        txtVat.on("change paste keyup", onUserInput);
        txtConditions.on("change paste keyup", onUserInput);

        $("#blade-content-"+_bladeName+" .buttonArrowListItem").on("click", function() {
            var editedTicket = getEditedTicket();
            _home.closeBlade("ticketing_configTicket_trad", function() {
                displayTranslationPanelState(true);
                _home.openBlade("ticketing_configTicket_trad",
                    [
                        editedTicket.nameTranslations,
                        editedTicket.conditionTranslations
                    ]);
            });
        });
    }

    function onUserInput() {
        var editedItem = getEditedTicket();
        var hasChanged = !_utilities.areArraysEqual(_original, editedItem);
        updateCommandButtonsState(hasChanged);
    }

    function onBladeClosed(bladeName) {
        if(bladeName == "ticketing_configTicket_trad") {
            displayTranslationPanelState(false);
        }
    }

    function getEditedTicket() {
        _editedItem.id = _original == null ? "" : _original.id;
        _editedItem.name = $("#ticketForm_name").val();
        _editedItem.price = parseFloat($("#ticketForm_price").val());
        _editedItem.vat = parseFloat($("#ticketForm_vat").val());
        _editedItem.condition = $("#ticketForm_conditions").val();
        return _editedItem;
    }

    function displayTranslationPanelState(opened) {
        $("#blade-content-"+_bladeName+" .buttonArrowListItem").toggleClass("buttonArrowListItemSelected", opened);
    }

    function updateCommandButtonsState(itemHasChanged) {
        $("#blade-" + _bladeName + "-commands-save").toggleClass('disabled', !itemHasChanged);
        $("#blade-" + _bladeName + "-commands-cancel").toggleClass('disabled', !itemHasChanged);
    }
});