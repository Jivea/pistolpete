/**
 * Created by Jivea
 * blade_ticketing.js
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var $ = require("jquery");
    var _app = require("app"),
        _home = require("home"),
        _bladeCommon = require("blade_common");

    var _bladeName = "ticketing";

    var _lastButtonSelected;

    exports.init = function() {
        _bladeCommon.init(_bladeName);

        renderBlade();
        _app.events.subscribe("blade-closed", onBladeClosed);
    };

    exports.unInit = function() {
        _app.events.unSubscribe("blade-closed", onBladeClosed);
    };

    function renderBlade() {
        var container = $("#blade-content-" + _bladeName);
        var content = require("hbs!templates/ticketing")();

        container.empty();
        container.append(content);

        $(".buttonMat").on("click", function() {
            onButtonClicked($(this));
        });
    }

    function onButtonClicked(clickedItem) {
        closeChildBlades(function() {
            selectButton(clickedItem);
        });
    }

    function selectButton(clickedItem) {
        displayButtonSelection(clickedItem);

        if(clickedItem != null) {
            var command = clickedItem.attr("data-command");
            _home.openBlade("ticketing_" + command);
        }
    }

    function displayButtonSelection(item) {
        if(_lastButtonSelected != null) {
            _lastButtonSelected.toggleClass("buttonMatSelected", false);
        }
        if(item != null) {
            item.toggleClass("buttonMatSelected", true);
        }
        _lastButtonSelected = item;
    }

    function closeChildBlades(callback) {
        if(_lastButtonSelected) {
            var bladeToClose = "ticketing_" + _lastButtonSelected.attr("data-command");
            _home.closeBlade(bladeToClose, function() {
                if(callback) callback();
            });
        } else {
            if(callback) callback();
        }
    };

    function onBladeClosed(bladeName) {
        if(bladeName == "ticketing_configTicket" || bladeName == "ticketing_configTax") {
            displayButtonSelection(null);
        }
    }
});