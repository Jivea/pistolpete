/**
 * Created by Jivea
 * sessionHelper.js
 * + module handling session management
 */

/* global define */
define(function(require, exports, module) {
    "use strict";
    var _tokenKey = "cineven_login_tokenKey";
    var _loggedIn = false;
    var _realSessionHelper = require("sessionHelper");
    var _timeSimulation = 1000;
    var _fakeLogin = "John Doe Mc Cormik";

    exports.isUserLoggedIn = function() {
        return _loggedIn;
    };

    exports.getSessionToken = function() {
        return 'fakeSessionToken';
    };

    exports.getUserInfo = function(onSuccess, onError) {
        onSuccess({
            UserName:  _fakeLogin,
            Name:      "Vanhoof",
            FirstName: "Jérôme",
            Email:     "Jérôme"
        });
    };

    exports.signUp = function(signUpRequest, onSuccess, onError) {
        _loggedIn = true;
        setTimeout(function(){onSuccess(null);}, _timeSimulation);
    };

    exports.signIn = function(signInRequest, onSuccess, onError) {
        _loggedIn = true;
        setTimeout(function(){onSuccess(null);}, _timeSimulation);
    };

    exports.signOut = function(onSuccess, onError) {
        _loggedIn = false;
        setTimeout(function(){onSuccess(null);}, _timeSimulation);
    };

    exports.buildSignUpRequest = _realSessionHelper.buildSignUpRequest;
    exports.buildSignInRequest = _realSessionHelper.buildSignInRequest;
});