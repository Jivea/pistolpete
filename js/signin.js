/**
 * Created by Jivea
 * signin.js
 * + Application module running against signin.hbs
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var $ = require("jquery");
    var _app = require("app");
    var _sessionHelper = require("sessionHelperX");

    exports.init = function() {
        $("#btnSignIn").on("click", function() {
            var userName = $("#txtSignInLogin").val();
            var password = $("#txtSignInPassword").val();

            _app.enableInputs(false);
            performSignIn(userName, password);
        });

        $("#lblSignInGoToSignUp").on("click", function() {
            _app.navigate("signup");
        });
    };

    function performSignIn(userName, password) {
        _app.onWaitRequired();

        var signInRequest = _sessionHelper.buildSignInRequest(userName, password);
        _sessionHelper.signIn(signInRequest, function(token) {
            console.log("token retrieved:" + token);
            _app.onRequiredWaitEnded();
            _app.navigate("home");
        }, function(error) {

            var errorTitle = "Could not sign in! ";
            var errorMessage = getErrorMessage(error);

            _app.onRequiredWaitEnded();
            _app.onMessageError(errorTitle, errorMessage);
            _app.enableInputs(true);
        });
    }

    function getErrorMessage(error) {
        if(error.status == 400) {
            if(error.responseJSON != null && error.responseJSON.error == "invalid_grant"){
                return "Invalid user name or password!";
            }
        } else {
            return $(error.responseText).filter("title").text();
        }
    }
});