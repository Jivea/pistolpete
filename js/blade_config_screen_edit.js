/**
 * Created by Jivea
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var Screen = require("models/screen");

    var $ = require("jquery");
    var _app = require("app");
    var _proxy = require("configScreenProxyX");
    var _bladeDefinitions = require("blades/bladeDefinitions");
    var _utilities = require("utilities");
    var _bladeCommon = require("blade_common");

    var _isNew;
    var _bladeName;
    var _original;
    
    exports.init = function(dataContext) {
        _isNew = dataContext == null;
        _original = dataContext;
        _bladeName = _isNew ? "configScreen_new" : "configScreen_edit";

        _bladeCommon.init(_bladeName, _isNew ? null : _original.Name);

        initScreenEdition();
    };

    exports.onCommandSave = function() {
        updateCommandButtonsState(false);

        var editedItem = getEditedScreen();
        _proxy.saveScreen(editedItem, function(savedItem) {
            _app.onSuccess("New screen successfully added.");
            _app.events.publish("configScreen-saved", savedItem);
        }, function(error) {
            _app.onError(error);
        });
    };

    exports.onCommandUpdate = function() {
        updateCommandButtonsState(false);

        var editedItem = getEditedScreen();
        _proxy.saveScreen(editedItem, function(savedItem) {
            _app.onSuccess("Screen successfully updated.");
            _app.events.publish("configScreen-updated", savedItem);
        }, function(error) {
            _app.onError(error);
        });
    };

    exports.onCommandDelete = function() {
        _proxy.deleteScreen(_original, function() {
            _app.onSuccess("Screen successfully deleted");
            _app.events.publish("configScreen-deleted");
        }, function(error) {
            _app.onError(error);
        });
    };

    exports.onCommandCancel = function() {
        initScreenEdition();
    };

    function initScreenEdition() {
        loadForm();
        updateCommandButtonsState(false);
        bindForm(_original);
    }

    function loadForm() {
        var container = $("#blade-content-" + _bladeName);
        var form = require("hbs!templates/screenForm")();

        container.empty();
        container.append(form);
    }

    function bindForm(item) {

        var txtName = $("#screenForm_name");
        var txtSeats = $("#screenForm_seatcount");

        if(item != null) {
            txtName.val(item.name);
            txtSeats.val(item.seatCount);
        }

        txtName.on("change paste keyup", onUserInput);
        txtSeats.on("change paste keyup", onUserInput);
    }

    function onUserInput() {
        var editedItem = getEditedScreen();
        var hasChanged = !_utilities.areArraysEqual(_original, editedItem);
        updateCommandButtonsState(hasChanged);
    }

    function getEditedScreen() {

        return new Screen(
            _original == null ? "" : _original.id,
            $("#screenForm_name").val(),
            $("#screenForm_seatcount").val(),
            _original == null ? _bladeDefinitions.getBlade("configScreen").getNextDisplayRank() :  _original.displayRank);
    }

    function updateCommandButtonsState(itemHasChanged) {
        $("#blade-" + _bladeName + "-commands-save").toggleClass('disabled', !itemHasChanged);
        $("#blade-" + _bladeName + "-commands-cancel").toggleClass('disabled', !itemHasChanged);
    }
});