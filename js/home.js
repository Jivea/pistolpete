/**
 * Created by Jivea
 * home.js
 * + Application module running against home.hbs
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var $ = require("jquery");
    var _bladeManager = require("blading/bladeManager");
    var _sessionHelper = require("sessionHelperX");
    var _app = require("app");

    var _lastMenuItemSelected;

    exports.init = function() {
        _bladeManager.init();

        $("#mainMenu").find("li").on("click", function() {
            var clickedItem = $(this);
            displaySelectMenuItem(clickedItem);
            openBlade(clickedItem.attr("data-command-name"));
        });

        _sessionHelper.getUserInfo(function(userInfo) {
            loadUserHub(userInfo);
        }, function(error) {
            _app.onError(error);
        });

        _app.events.subscribe("blade-closebtn-clicked", onCloseBladeClicked);
        _app.events.subscribe("blade-closed", onBladeClosed);
    };
    exports.openBlade = openBlade;
    exports.closeBlade = closeBlade;

    function openBlade(bladeName, dataContext) {
        _bladeManager.showBlade($("#mainPlaceHolder"), bladeName, dataContext);
    }

    function closeBlade(bladeName, callback) {
        _bladeManager.closeBlade(bladeName, function(closedBladeNames) {
            closedBladeNames.forEach(function(closedBladeName) {
                _app.events.publish("blade-closed", closedBladeName)
            });

            if(callback) {
                callback();
            }
        });
    }

    function onCloseBladeClicked(bladeName) {
        closeBlade(bladeName);
    }
    function onBladeClosed(bladeName) {
        if(_lastMenuItemSelected !=null) {
            if(_lastMenuItemSelected.attr("data-command-name") == bladeName) {
                displaySelectMenuItem(null);
            }
        }
    }

    function displaySelectMenuItem(menuItem) {
        if(_lastMenuItemSelected != null) {
            _lastMenuItemSelected.toggleClass("mainMenuItemSelected", false);
        }
        if(menuItem != null) {
            menuItem.toggleClass("mainMenuItemSelected", true);
        }
        _lastMenuItemSelected = menuItem;
    }

    function loadUserHub(userInfo) {
        var template = require("hbs!templates/userhub");
        var content = template(userInfo);

        var element = $("#userHub");
        element.empty();
        element.append(content);

        var userHubJs = require("userhub");
        userHubJs.init();
    }

});