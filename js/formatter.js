/**
 * Created by Jivea
 * formatter.js
 * +
 */
/* global define */
define(function(require, exports, module) {
    "use strict";

    var moment = require("moment");

    exports.formatDateToYYYYMMDD = function(datetime) {
        if(datetime == "undefined" || typeof(datetime) == "undefined") {
            return "Unknown";
        }
        return moment(datetime).format("YYYY/MM/DD");
    };

    exports.formatMomentToLT = function(momentTime) {
        if(!momentTime) {
            return undefined;
        }

        return momentTime.format("HH:mm");
    };

    exports.formatMomentToFriendlyDate = function (momentDate) {
        if (!momentDate) {
            return undefined;
        }

        return momentDate.format("dddd LL");
    };
   
});