/**
 * Created by Jivea
 * main.js
 * Main application entry point.
 * + Configuration and initialization of require.js.
 * + Configuration of handlebars helpers
 * + Configuration of momentjs locale
 * + Load and init "app" module.
 */

/* global require */
(function() {
    "use strict";

    var noDbMode = true;

    require.config({
        paths: {
            //jquery:     "libs/jquery-2.1.3.min",
            jquery:     "libs/jquery-ui-1.11.4.custom/external/jquery/jquery",
            jqueryui:   "libs/jquery-ui-1.11.4.custom/jquery-ui",
            underscore: "libs/hbs/underscore",
            handlebars: "libs/hbs/handlebars",
            hbs:        "libs/hbs",
            templates:  "../templates",
            spin:       "libs/spin",
            moment:     "libs/moment"
        },
        map:   {
            '*': noDbMode ? getNoDbModeMapping() : getMapping()
        }
    });

    require(["handlebarsHelpers"], function(handlebarsHelpers) {
        handlebarsHelpers.registerHelpers();
    });

    require(["moment", "libs/locale/fr"], function(moment, fr) {
        moment.locale("fr");
    });

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };

    Array.prototype.getIndexWithId = function (itemId) {
        for(var i = 0; i < this.length; i++) {
            if(this[i].id == itemId)
                return i;
        }
        return -1;
    };

    Array.prototype.getObjectWithId = function (itemId) {
        for(var i = 0; i < this.length; i++) {
            if(this[i].id == itemId)
                return this[i];
        }
        return null;
    };

    require(["app", "jqueryui"], function(app) {    // load once jquery ui for datetimepickers
        app.init();
    });
})();

function getMapping() {
    return {

        'hbs/underscore':           'underscore',
        'hbs/handlebars':           'handlebars',
        'sessionHelperX':           'sessionHelper',
        'bookingProxyX':            'proxies/proxyBooking',
        'configScreenProxyX':       'proxies/proxyConfigScreen',
        'movieDescriptionProxyX':   'proxies/proxyMovieDescription',
        'schedulingProxyX' :        'proxies/proxyScheduling',
        'ticketingProxyX':          'proxies/proxyTicketingFake',
        'ticketingTaxProxyX':       'proxies/proxyTicketingTaxFake'
    };
}

function getNoDbModeMapping() {
    return {
        'hbs/underscore':           'underscore',
        'hbs/handlebars':           'handlebars',

        'sessionHelperX':         '_fakeSessionHelper',
        'bookingProxyX':          'proxies/proxyBookingFake',
        'configScreenProxyX':     'proxies/proxyConfigScreenFake',
        'movieDescriptionProxyX': 'proxies/proxyMovieDescriptionFake',
        'schedulingProxyX':       'proxies/proxySchedulingFake',
        'ticketingProxyX':        'proxies/proxyTicketingFake',
        'ticketingTaxProxyX':     'proxies/proxyTicketingTaxFake'
    };
}

