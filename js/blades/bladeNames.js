/**
 * Created by Jivea
 * +
 */

/* global define */
define(function () {
    "use strict";

    return {
        THEATER: "theater",
        BOOKING: "booking",
        BOOKING_NEW: "booking_new",
        BOOKING_EDIT: "booking_edit",
        BOOKING_DESCRIPTION: "booking_description",
        SCHEDULING: "scheduling",
        SCHEDULING_NEW: "scheduling_new",
        SCHEDULING_EDIT: "scheduling_edit",
        TICKETING: "ticketing",
        TICKETING_CONFIGTICKET: "ticketing_configTicket",
        TICKETING_CONFIGTICKET_NEW: "ticketing_configTicket_new",
        TICKETING_CONFIGTICKET_EDIT: "ticketing_configTicket_edit",
        TICKETING_CONFIGTICKET_TRAD: "ticketing_configTicket_trad",
        TICKETING_CONFIGTAX: "ticketing_configTax",
        TICKETING_CONFIGTAX_EDIT: "ticketing_configTax_edit",
        CONFIGSCREEN: "configScreen",
        CONFIGSCREEN_NEW: "configScreen_new",
        CONFIGSCREEN_EDIT: "configScreen_edit"
    }
});