/**
 * Created by Jivea
 * booking.js
 * +
 */

/* global define */
define(["jquery", "../home", "blading/bladeEdit", "models/booking", "bookingProxyX"],
    function($, _home, BladeEdit, Booking, _bookingProxy) {
    "use strict";

    function BladeBookingEdit(bladeConfiguration, template, isNew) {
        var self = this;

        BladeEdit.call(this, bladeConfiguration, template, isNew);

        this.lastDescriptionItemSelected = null;

        this.cancel = function() {
            if(self.lastDescriptionItemSelected != null) {
                selectDescription(null);
                _home.closeBlade("booking_description");
            }
            BladeEdit.prototype.cancel.call(this);
        };

        this.onDescriptionDeleted = function(description) {
            delete self.editedItem.descriptions[description.lang];
            $("#descriptionTitle-" + description.lang).text("(pas de description)");
            self.onEditedItemChanged.call(self);

            selectDescription(null);
            _home.closeBlade("booking_description");
        };
        this.onDescriptionUpdated = function(description) {
            self.editedItem.descriptions[description.lang] = description;
            $("#descriptionTitle-" + description.lang).text(description.title ? description.title : "(pas de description)");
            self.onEditedItemChanged.call(self);
        };

        this.registerDOMEvents = function() {
            var configFrom = getDatePickerConfig();
            configFrom.onClose = function(selectedDate) {
                $("#bookingForm_to").datepicker("option", "minDate", selectedDate);
            };

            var configTo = getDatePickerConfig();
            configTo.onClose = function(selectedDate) {
                $("#bookingForm_from").datepicker("option", "maxDate", selectedDate);
            };

            $("#bookingForm_from").datepicker(configFrom);
            $("#bookingForm_to").datepicker(configTo);

            $(".buttonArrowListItem").on("click", function() {
                onDescriptionClicked($(this));
            });
        };


        function onDescriptionClicked(clickedItem) {
            closeChildBlades(function() {
                selectDescription(clickedItem);
            });
        }

        function selectDescription(clickedItem) {
            displayDescriptionSelection(clickedItem);

            if(clickedItem != null) {
                var lang = clickedItem.attr("data-descriptionlang");
                openChildBlade("booking_description", lang);
            }
        }

        function displayDescriptionSelection(item) {
            if(self.lastDescriptionItemSelected != null) {
                self.lastDescriptionItemSelected.toggleClass("buttonArrowListItemSelected", false);
            }
            if(item != null) {
                item.toggleClass("buttonArrowListItemSelected", true);
            }
            self.lastDescriptionItemSelected = item;
        }

        function openChildBlade(bladeName, lang) {
            _home.openBlade(bladeName,
                {
                    booking: self.getEditedItem(),
                    lang:    lang
                });
        }

        function closeChildBlades(callback) {
            if(self.lastDescriptionItemSelected != null) {
                _home.closeBlade("booking_description", function() {
                    selectDescription(null);
                    if(callback) callback();
                });
            } else {
                _home.closeBlade("booking_schedule", function() {
                    if(callback) callback();
                });
            }
        }

        function getDatePickerConfig() {
            return {
                changeMonth:       true,
                changeYear:        true,
                showOtherMonths:   true,
                selectOtherMonths: true,
                showButtonPanel:   true,
                showWeek:          true,
                firstDay:          1,
                minDate:           null,
                dateFormat:        'dd/mm/yy'
            };
        }
    }

    BladeBookingEdit.prototype = Object.create(BladeEdit.prototype);
    BladeBookingEdit.prototype.constructor = BladeBookingEdit;
    BladeBookingEdit.prototype.getModelType = function() {
        return Booking;
    };
    BladeBookingEdit.prototype.getModelProxy = function() {
        return _bookingProxy;
    };

    BladeBookingEdit.prototype.init = function(dataContext) {
        BladeEdit.prototype.init.call(this, dataContext);

        if(!this.isNewItem) {
            this.setBladeTitle(this.originalItem.movieTitle);
        }
    };

    BladeBookingEdit.prototype.getAppEventsToSubscribe = function() {
        var appEvents = BladeEdit.prototype.getAppEventsToSubscribe.call(this);
        appEvents.push({name:"booking-description-changed", handler: this.onDescriptionUpdated});
        appEvents.push({name:"booking-description-deleted", handler: this.onDescriptionDeleted});
        return appEvents;
    };

    BladeBookingEdit.prototype.bindForm = function(item) {
        var self = this;
        // booking
        var pickerFrom = $("#bookingForm_from");
        var pickerTo = $("#bookingForm_to");
        var txtMovie = $("#bookingForm_movieTitle");
        var txtPct = $("#bookingForm_pct");

        if(item != null) {
            txtMovie.val(item.movieTitle);
            txtPct.val(item.distributorPercentage);
            pickerFrom.datepicker("setDate", item.startDate);
            pickerTo.datepicker("setDate", item.endDate);
        }

        txtMovie.on("change paste keyup", function() { self.onEditedItemChanged.call(self);});
        txtPct.on("change paste keyup", function() { self.onEditedItemChanged.call(self);});
        pickerFrom.on("change paste keyup", function() { self.onEditedItemChanged.call(self);});
        pickerTo.on("change paste keyup", function() { self.onEditedItemChanged.call(self);});

        // descriptions
        var buttons = $("#blade-content-" + self.getBladeName()).find(".buttonArrowListItem");
        for(var i = 0; i < buttons.length; i++) {
            var lang = $(buttons[i]).attr("data-descriptionlang");
            var localizedTitle = "(pas de description)";
            if(item.descriptions[lang]) {
                localizedTitle = item.descriptions[lang].title;
            }
            $(buttons[i]).find("p").text(localizedTitle);
        }
    };

    BladeBookingEdit.prototype.getEditedItem = function() {
        this.editedItem.id = this.originalItem.id || null;
        this.editedItem.movieTitle = $("#bookingForm_movieTitle").val();
        this.editedItem.startDate = $("#bookingForm_from").datepicker('getDate');
        this.editedItem.endDate = $("#bookingForm_to").datepicker('getDate');
        this.editedItem.distributorPercentage = parseFloat($("#bookingForm_pct").val()) || 0;

        return this.editedItem;
    };

    BladeBookingEdit.prototype.subscribeDOMEvents = function() {
        this.registerDOMEvents();
    };

    return BladeBookingEdit;
});
