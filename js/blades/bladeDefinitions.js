/**
 * Created by Jivea
 * bladesCollection.js
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var $ = require("jquery");
    var BladeNames = require("blades/bladeNames");
    var BladeConfiguration = require("blading/bladeConfiguration");
    var BladeCmdType = require("blading/bladeConfigurationCommandType");

    var _app = require("app");


    //var _jsSchedulingEdit = require("blade_scheduling_edit");
    //var _jsTicketing = require("blade_ticketing");
    //var _jsTicketConfig = new (require("blade_list"))(BladeNames.TICKETING_CONFIGTICKET, require("hbs!templates/ticketing_configTicket"), require("ticketingProxyX"));
    //var _jsTicketConfigEdit = require("blade_ticketing_ticket_edit");
    //var _jsTicketConfigTrad = require("blade_ticketing_ticket_trad");
    //var _jsTicketTax = new (require("blade_list"))(BladeNames.TICKETING_CONFIGTAX, require("hbs!templates/ticketing_configTax"), require("ticketingTaxProxyX"));
    //var _jsTicketTaxEdit = require("blade_ticketing_tax_edit");
    //var _jsConfigScreen = new (require("blade_list"))(BladeNames.CONFIGSCREEN, require("hbs!templates/config_screen"), require("configScreenProxyX"));
    //var _jsConfigScreenEdit = require("blade_config_screen_edit");

    var _bladeConfigurations = [
        // First-level blades do not have a "_" in their name
        // Second-level blades do have a "_" on their name
        // i.e.: "booking" --> first-level. "booking_new" --> second-level
        new BladeConfiguration(BladeNames.THEATER, "Cinéma", "home", "500px"),

        new BladeConfiguration(BladeNames.BOOKING, "Réservations", "film", "500px", [BladeCmdType.ADD]),
        new BladeConfiguration(BladeNames.BOOKING_NEW, "Ajout d'une réservation", "plus", "300px", [BladeCmdType.SAVE]),
        new BladeConfiguration(BladeNames.BOOKING_EDIT, "Modification d'une réservation", "pencil", "300px", [BladeCmdType.SAVE, BladeCmdType.DELETE, BladeCmdType.CANCEL]),
        new BladeConfiguration(BladeNames.BOOKING_DESCRIPTION, "Description", "pencil", "400px", [BladeCmdType.DELETE, BladeCmdType.CANCEL]),

        new BladeConfiguration(BladeNames.SCHEDULING, "Séances", "calendar", "800px", [BladeCmdType.ADD]),
        new BladeConfiguration(BladeNames.SCHEDULING_NEW, "Ajout de séance", "plus", "300px", [BladeCmdType.SAVE]),
        new BladeConfiguration(BladeNames.SCHEDULING_EDIT, "Modification de la séance", "pencil", "300px", [BladeCmdType.SAVE, BladeCmdType.DELETE, BladeCmdType.CANCEL])

        //
        //new BladeConfiguration(BladeNames.TICKETING, _jsTicketing, "Tickets", "ticket", "250px"),
        //new BladeConfiguration(BladeNames.TICKETING_CONFIGTICKET, _jsTicketConfig, "Configuration des tickets", "ticket", "500px", [BladeCmdType.ADD]),
        //new BladeConfiguration(BladeNames.TICKETING_CONFIGTICKET_NEW, _jsTicketConfigEdit, "Ajout d'un ticket", "plus", "300px", [BladeCmdType.SAVE]),
        //new BladeConfiguration(BladeNames.TICKETING_CONFIGTICKET_EDIT, _jsTicketConfigEdit, "Modification du ticket", "pencil", "300px", [BladeCmdType.SAVE, BladeCmdType.DELETE, BladeCmdType.CANCEL]),
        //new BladeConfiguration(BladeNames.TICKETING_CONFIGTICKET_TRAD, _jsTicketConfigTrad, "Traductions", "globe", "300px"),
        //new BladeConfiguration(BladeNames.TICKETING_CONFIGTAX, _jsTicketTax, "Configuration des taxes", "legal", "500px"),
        //new BladeConfiguration(BladeNames.TICKETING_CONFIGTAX_EDIT, _jsTicketTaxEdit, "Édition de la taxe", "pencil", "300px", [BladeCmdType.SAVE, BladeCmdType.CANCEL]),
        //
        //new BladeConfiguration(BladeNames.CONFIGSCREEN, _jsConfigScreen, "Configuration des écrans", "cogs", "500px", [BladeCmdType.ADD, BladeCmdType.UP, BladeCmdType.DOWN]),
        //new BladeConfiguration(BladeNames.CONFIGSCREEN_NEW, _jsConfigScreenEdit, "Ajout d'un écran", "plus", "300px", [BladeCmdType.SAVE]),
        //new BladeConfiguration(BladeNames.CONFIGSCREEN_EDIT, _jsConfigScreenEdit, "Modification d'un écran", "pencil", "300px", [BladeCmdType.SAVE, BladeCmdType.DELETE, BladeCmdType.CANCEL])

    ];
    var BladeTheater = require("blades/blade_theater"),
        BladeList = require("blading/blade_list"),
        BladeBookingEdit = require("blades/blade_booking_edit"),
        BladeBookingDescription = require("blades/blade_booking_description"),
        BladeScheduling = require("blade_scheduling"),
        BladeSchedulingEdit = require("blade_scheduling_edit");

    var _blades = [
        new BladeTheater(getBladeConfiguration(BladeNames.THEATER), require("hbs!templates/theater")),
        new BladeList(getBladeConfiguration(BladeNames.BOOKING), require("hbs!templates/bookingList"), require("bookingProxyX")),
        new BladeBookingEdit(getBladeConfiguration(BladeNames.BOOKING_NEW), require("hbs!templates/bookingEdit"), true),
        new BladeBookingEdit(getBladeConfiguration(BladeNames.BOOKING_EDIT), require("hbs!templates/bookingEdit"), false),
        new BladeBookingDescription(getBladeConfiguration(BladeNames.BOOKING_DESCRIPTION), require("hbs!templates/bookingDescription")),
        new BladeScheduling(getBladeConfiguration(BladeNames.SCHEDULING), require("hbs!templates/scheduling")),
        new BladeSchedulingEdit(getBladeConfiguration(BladeNames.SCHEDULING_NEW), require("hbs!templates/scheduling_edit"), true),
        new BladeSchedulingEdit(getBladeConfiguration(BladeNames.SCHEDULING_EDIT), require("hbs!templates/scheduling_edit"), false)
    ];

    exports.getBlade = getBlade;
    exports.getBladeConfiguration = getBladeConfiguration;

    exports.isBladeDefined = function(bladeName) {
        return getBladeConfiguration(bladeName) != null && getBlade(bladeName) != null;
    };
    exports.isFirstLevelBlade = function(bladeName) {
        return bladeName.indexOf("_") === -1;
    };

    function getBladeConfiguration(bladeName) {
        for(var i = 0; i < _bladeConfigurations.length; i++) {
            if(_bladeConfigurations[i].name === bladeName) {
                return _bladeConfigurations[i];
            }
        }
        return null;
    }

    function getBlade(bladeName) {
        for(var i = 0; i < _blades.length; i++) {
            var blade = _blades[i];
            if(blade.getConfiguration().name === bladeName) {
                return _blades[i];
            }
        }
        return null;
    }
});