/**
 * Created by Jivea
  * +
 */

/* global define */
define(["jquery", "../app", "home", "blading/bladeEdit", "models/movieFullDescription", "movieDescriptionProxyX", "utilities", "hbs!templates/bookingDescriptionResults", "hbs!templates/bookingDescriptionResultsNone"],
    function($, _app, _home, BladeEdit, MovieFullDescription, _movieDescriptionProxy, _utilities, templateResults, templateNoResult) {
        "use strict";

        function BladeBookingDescription(bladeConfiguration, template) {
            var self = this,
                copyInfoInProgress = false,
                foundMovies = [];

            this.lang = null;
            this.booking = null;

            BladeEdit.call(this, bladeConfiguration, template, false);

            this.registerDOMEvents = function() {
                $("#bookingDescription_search").on("click", onSearchMovieButtonClicked);
                $("#bookingDescription_AcceptFoundMovie").on("click", onAcceptMovieButtonClicked);

                $("#bookingDescription_title").on("change paste keyup", onUserInput);
                $("#bookingDescription_originalTitle").on("change paste keyup", onUserInput);
                $("#bookingDescription_resume").on("change paste keyup", onUserInput);
                $("#bookingDescription_synopsis").on("change paste keyup", onUserInput);
                $("#bookingDescription_duration").on("change paste keyup", onUserInput);
                $("#bookingDescription_director").on("change paste keyup", onUserInput);
                $("#bookingDescription_casting").on("change paste keyup", onUserInput);
                $("#bookingDescription_distributor").on("change paste keyup", onUserInput);
                $("#bookingDescription_releaseDate").on("change paste keyup", onUserInput);
            };

            function onUserInput() {
                self.onEditedItemChanged.call(self);
            }

            this.delete = function() {
                var editedDescription = this.getEditedItem();
                _app.events.publish("booking-description-deleted", editedDescription);
            };

            function onSearchMovieButtonClicked() {
                updateSearchButton(true);
                var searchText = $("#bookingDescription_searchText").val();
                _movieDescriptionProxy.getMovieSourceItems(self.lang, searchText, function(descriptions) {
                    foundMovies = descriptions;

                    var results;
                    if(foundMovies.length > 0) {
                        results = templateResults(foundMovies);
                    } else {
                        results = templateNoResult("hbs!templates/bookingDescriptionResultsNone");
                    }
                    fillSearchResults(results);

                }, function(error) {
                    fillSearchResults(error);
                });
            }

            function fillSearchResults(content) {
                var container = $("#bookingDescription_searchResult");
                container.empty();
                container.append(content);

                updateCopyMovieInfoButton();
                updateSearchButton(false);
            }

            function onAcceptMovieButtonClicked() {
                copyInfoInProgress = true;
                updateCopyMovieInfoButton();

                var selectedFilm = $("#bookingDescriptionResults_combo").val();
                _movieDescriptionProxy.getMovieSourceItemInfo(self.lang, selectedFilm, function(movieDescription) {
                    self.bindForm(movieDescription);
                    copyInfoInProgress = false;
                    updateCopyMovieInfoButton();
                    self.onEditedItemChanged();
                }, function(error) {
                    copyInfoInProgress = false;
                    updateCopyMovieInfoButton();
                    _app.onError(error);
                });
            }

            function updateCopyMovieInfoButton() {
                $("#bookingDescription_AcceptFoundMovie").toggleClass("disabled", copyInfoInProgress || foundMovies.length <= 0);
                $("#bookingDescription_AcceptFoundMovie_Spinner").toggleClass("hidden", !copyInfoInProgress);
            }

            function updateSearchButton(searchInProgress) {
                $("#bookingDescription_search").toggleClass("disabled", searchInProgress);
                $("#bookingDescription_search_Spinner").toggleClass("hidden", !searchInProgress);
            }
        }

        BladeBookingDescription.prototype = Object.create(BladeEdit.prototype);
        BladeBookingDescription.prototype.constructor = BladeBookingDescription;
        BladeBookingDescription.prototype.getModelType = function() {
            return MovieFullDescription;
        };
        BladeBookingDescription.prototype.getModelProxy = function() {
            return null;
        };

        BladeBookingDescription.prototype.init = function(dataContext) {
            this.lang = dataContext.lang;
            this.booking = dataContext.booking;

            if(this.booking.descriptions && this.booking.descriptions[this.lang]) {
                dataContext.item = MovieFullDescription.createCopy(this.booking.descriptions[this.lang]);
            } else {
                dataContext.item = null;
            }

            this.userInputReceived = false;

            BladeEdit.prototype.init.call(this, dataContext);
            this.setBladeTitle("Description (" + this.lang + ")");

            $("#bookingDescription_searchText").val(this.booking.movieTitle);
        };

        BladeBookingDescription.prototype.bindForm = function(item) {
            var self = this;

            if(item != null) {
                $("#bookingDescription_title").val(item.title);
                $("#bookingDescription_originalTitle").val(item.originalTitle);
                $("#bookingDescription_resume").val(item.resume);
                $("#bookingDescription_synopsis").val(item.synopsis);
                $("#bookingDescription_duration").val(item.duration);
                $("#bookingDescription_director").val(item.director);
                $("#bookingDescription_casting").val(item.casting);
                $("#bookingDescription_distributor").val(item.distributor);
                $("#bookingDescription_releaseDate").val(item.releaseDate);
            } else {
                $("#blade-content-" + self.getBladeName()).find("input, textarea").val("");
            }
        };

        BladeBookingDescription.prototype.getEditedItem = function() {
            var self = this;

            var description = self.booking.descriptions[self.lang] || new MovieFullDescription(self.lang);
            description.identifier = self.originalItem == null ? "" : self.originalItem.identifier;
            description.originalTitle = $("#bookingDescription_originalTitle").val();
            description.title = $("#bookingDescription_title").val();
            description.releaseDate = $("#bookingDescription_releaseDate").val();
            description.resume = $("#bookingDescription_resume").val();
            description.synopsis = $("#bookingDescription_synopsis").val();
            description.duration = $("#bookingDescription_duration").val();
            description.distributor = $("#bookingDescription_distributor").val();
            description.casting = $("#bookingDescription_casting").val();
            description.director = $("#bookingDescription_director").val();
            return description;
        };

        BladeBookingDescription.prototype.onEditedItemChanged = function() {
            BladeEdit.prototype.onEditedItemChanged.call(this);
        };

        BladeBookingDescription.prototype.subscribeDOMEvents = function() {
            this.registerDOMEvents();
        };

        BladeBookingDescription.prototype.getEventPrefix = function() {
            return "booking-description";
        };

        return BladeBookingDescription;
    });