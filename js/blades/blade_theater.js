/**
 * Created by Jivea
  * +
 */

/* global define */
define(["jquery", "../app", "blading/blade"], function($, app, Blade) {
    "use strict";

    function BladeTheater(bladeConfiguration, template) {
       Blade.call(this, bladeConfiguration, template);
    }

    BladeTheater.prototype = Object.create(Blade.prototype);
    BladeTheater.prototype.constructor = BladeTheater;

    return BladeTheater;
});