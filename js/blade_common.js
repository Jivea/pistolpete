/**
 * Created by Jivea
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var $ = require("jquery");
    var _app = require("app");

    exports.init = function(bladeName, bladeTitle) {
        hookCloseBtn(bladeName);
        if(bladeTitle) {
            setBladeTitle(bladeName, bladeTitle);
        }
    };

    exports.showSpinner = showSpinner;

    function hookCloseBtn(bladeName) {
        var btn = $("#blade-" + bladeName).find(".blade-closebtn");
        btn.on("click", function() {
            _app.events.publish("blade-closebtn-clicked", bladeName);
        });
    }

    function setBladeTitle(bladeName, bladeTitle) {
        var headerLabel = $("#blade-" + bladeName).find(".blade-header-text");
        headerLabel.text(bladeTitle);
    }

    function showSpinner(bladeName, visible) {
        var spinner = $("#blade-" + bladeName).find(".blade-spinner");
        spinner.toggleClass("hidden", !visible);
    }

});