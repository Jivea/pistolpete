/**
 * Created by Jivea
 * signout.js
 * + Application module running against signout.hbs
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var $ = require("jquery");
    var _app = require("app");
    var _sessionHelper = require("sessionHelperX");

    exports.init = function() {
        performSignOut()
    };

    function performSignOut() {
        _app.onWaitRequired();
        _sessionHelper.signOut(function() {
            console.log("successfully Signed Out:");

            unloadUserHub();

            _app.onRequiredWaitEnded();
            _app.navigate("home");
        }, function(error) {
            _app.onError(error);
            _app.onRequiredWaitEnded();
        });
    }
    function unloadUserHub(){
        var element = $("#userHub");
        element.empty();
    }
});