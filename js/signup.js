/**
 * Created by Jivea
 * signup.js
 * + Application module running against signup.hbs
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var $ = require("jquery");
    var _app = require("app");

    var _sessionHelper = require("sessionHelperX");

    exports.init = function() {
        $("#btnSignUp").on("click", function() {
            _app.enableInputs(false);
            performSignUp(function(userName, password) {
                performSignIn(userName, password);
            });
        });

        $("#lblSignUpGoToSignIn").on("click", function() {
            _app.navigate("signin");
        });
    };

    function performSignUp(callback) {
        _app.onWaitRequired();

        var userName = $("#txtSignUpLogin").val();
        var name = $("#txtSignUpName").val();
        var firstName = $("#txtSignUpFirstName").val();
        var email = $("#txtSignUpEmail").val();

        var password = $("#txtSignUpPassword").val();
        var confirmPassword = $("#txtSignUpConfirmPassword").val();

        // Todo: Perform client validation on fields

        var signUpRequest = _sessionHelper.buildSignUpRequest(userName, name, firstName, email, password, confirmPassword);
        _sessionHelper.signUp(signUpRequest, function(token) {
            callback(signUpRequest.UserName, signUpRequest.Password);
        }, function(error) {

            _app.enableInputs(true);
            _app.onError(error);
            _app.onRequiredWaitEnded();
        });
    }

    function performSignIn(userName, password) {
        _app.onWaitRequired();

        var signInRequest = _sessionHelper.buildSignInRequest(userName, password);
        _sessionHelper.signIn(signInRequest, function(token) {
            console.log("token retrieved:" + token);
            _app.onRequiredWaitEnded();
            _app.onRegistrationCompleted();
        }, function(error) {
            _app.onRequiredWaitEnded();
            _app.onError(error);
            _app.enableInputs(true);
        });
    }
});