/**
 * Created by Jivea
 * userHub.js
 * + Application module userHub.hbs
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var Spinner = require("spin");
    var $ = require("jquery");

    var _opts = {
        lines:     5, // The number of lines to draw
        length:    30, // The length of each line
        width:     16, // The line thickness
        radius:    22, // The radius of the inner circle
        corners:   1, // Corner roundness (0..1)
        rotate:    57, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color:     '#FF6900', // #rgb or #rrggbb or array of colors
        speed:     0.7, // Rounds per second
        trail:     60, // Afterglow percentage
        shadow:    false, // Whether to render a shadow
        hwaccel:   false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex:    2e9, // The z-index (defaults to 2000000000)
        top:       '50%', // Top position relative to parent
        left:      '50%' // Left position relative to parent
    };

    var _spinner;

    exports.start = function() {
        var frontLayer = $("#frontLayer");
        frontLayer.css("visibility", "visible");

        if(!_spinner) {
            _spinner = new Spinner(_opts);
        } else {
            _spinner.stop();
        }
        _spinner.spin(frontLayer);
    };

    exports.stop = function() {
        $("#frontLayer").css("visibility", "hidden");
        if(_spinner) {
            _spinner.stop();
        }
    }
});