/**
 * Created by Jivea
 * toast.js
 * + Application module running against toast.hbs
 */

/* global define */
define(function(require, exports, module) {
    "use strict";
    var $ = require("jquery");
    var _errorTimeOut = 5000;
    var _successTimeOut = 3000;
    var _infoTimeOut = 3000;

    exports.showError = function(title, message) {
        showMessage("message-error", title, message);
        setTimeout(function() { closeMessage();}, _errorTimeOut);
    };

    exports.showInfo = function(title, message) {
        showMessage("message-info", title, message);
        setTimeout(function() { closeMessage();}, _successTimeOut);
    };

    exports.showSuccess = function(title, message) {
        showMessage("message-success", title, message);
        setTimeout(function() { closeMessage();}, _infoTimeOut);
    };

    function showMessage(messageType, title, message){
        var toastTemplate = require("hbs!templates/toast");
        var toast = toastTemplate({
            Title: title,
            Message: message
        });

        $("body").append(toast);

        var item = $(".message");
        item.addClass(messageType);
        item.on("click", function(){
            closeMessage();
        });
    }
    function closeMessage() {
        $(".message").remove();
    }
});