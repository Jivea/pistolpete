/**
 * Created by Jivea
 * events.js
 * + Application module running against index.html
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var _listeners = {};

    exports.publish = function(eventName, data) {
        if(_listeners[eventName] === undefined)
            return ;

        for(var i=0 ; i<_listeners[eventName].length ;i++) {
            _listeners[eventName][i](data);
        }
    };

    exports.subscribe = function(eventName, listener) {
        if(_listeners[eventName] === undefined)
            _listeners[eventName] = [] ;
        _listeners[eventName].push(listener);
    };

    exports.unSubscribe = function(eventName, listener) {
        if(_listeners[eventName] === undefined)
           return false;

        for(var i=0 ; i<_listeners[eventName].length ;i++) {
            if(_listeners[eventName][i] == listener) {
                _listeners[eventName].splice(i,1);
                return true
            }
        }
        return false;
    };
});