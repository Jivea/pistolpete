/**
 * Created by Jivea
 * +
 */

/* global define */
define([], function() {
    "use strict";
    var moment = require("moment");

    function Scheduling(id, bookingId, movieTitle, screenId, scheduleTime) {
        this.id = id;
        this.bookingId = bookingId;
        this.movieTitle = movieTitle;
        this.screenId = screenId;
        this.scheduleTime = scheduleTime;
    }

    Scheduling.createCopy = function(scheduling) {
        var copy = new Scheduling(scheduling.id,
            scheduling.bookingId,
            scheduling.movieTitle,
            scheduling.screenId,
            scheduling.scheduleTime);
        return copy;
    };

    Scheduling.fromApiJson = function(jsonObject) {
        return new Scheduling(jsonObject.Id,
            jsonObject.BookingId,
            jsonObject.MovieTitle,
            jsonObject.ScreenId,
            moment.utc(jsonObject.ScheduleTime));
    };

    return Scheduling;
});