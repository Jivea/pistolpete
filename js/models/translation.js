/**
 * Created by Jivea
 * +
 */

/* global define */
define([], function() {
    "use strict";

    function Translation(key, lang, text) {
        this.key = key;
        this.lang = lang;
        this.text = text;
    }

    Translation.createCopy = function(translation) {
        var copy = new Translation();
        copy.key = translation.key;
        copy.lang = translation.lang;
        copy.text = translation.text;
        return copy;
    };

    return Translation;
});