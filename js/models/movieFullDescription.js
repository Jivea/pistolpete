/**
 * Created by Jivea
 * +
 */

/* global define */
define([], function() {
    "use strict";

    function MovieFullDescription(lang) {
        if(!(this instanceof MovieFullDescription)) {
            throw new TypeError("MovieFullDescription constructor cannot be called as a function.");
        }

        this.identifier = null;
        this.originalTitle = null;
        this.lang = lang;
        this.title = null;
        this.releaseDate = null;
        this.resume = null;
        this.synopsis = null;
        this.duration = null;
        this.distributor = null;
        this.casting = null;
        this.director = null;
    }

    MovieFullDescription.create = function(identifier, originalTitle, lang, title, releaseDate, resume, synopsis, duration, distributor, casting, director) {
        var description = new MovieFullDescription(lang);
        description.identifier = identifier;
        description.originalTitle = originalTitle;
        description.title = title;
        description.releaseDate = releaseDate;
        description.resume = resume;
        description.synopsis = synopsis;
        description.duration = duration;
        description.distributor = distributor;
        description.casting = casting;
        description.director = director;
        return description;
    };

    MovieFullDescription.createCopy = function(description) {
        var copy = new MovieFullDescription(description.lang);
        copy.identifier = description.identifier;
        copy.originalTitle = description.originalTitle;
        copy.title = description.title;
        copy.releaseDate = description.releaseDate;
        copy.resume = description.resume;
        copy.synopsis = description.synopsis;
        copy.duration = description.duration;
        copy.distributor = description.distributor;
        copy.casting = description.casting;
        copy.director = description.director;
        return copy;
    };

    return MovieFullDescription;
});