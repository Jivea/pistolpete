/**
 * Created by Jivea
 * +
 */

/* global define */
define(["models/translation"], function(Translation) {
    "use strict";

    function Ticket(id, name, price, vat, condition, nameTranslations, conditionTranslations) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.vat = vat;
        this.condition = condition;
        this.nameTranslations = nameTranslations ? nameTranslations: initEmptyTranslations("name");
        this.conditionTranslations = conditionTranslations ? conditionTranslations : initEmptyTranslations("condition");


        function initEmptyTranslations(key) {
            var translations =  [];
            translations.push(new Translation(key, "fr", ""));
            translations.push(new Translation(key, "en", ""));
            translations.push(new Translation(key, "nl", ""));
            translations.push(new Translation(key, "de", ""));
            return translations;
        }
    }

    Ticket.createCopy = function(ticket) {
        var copy = new Ticket();
        copy.id = ticket.id;
        copy.name = ticket.name;
        copy.price = ticket.price;
        copy.vat = ticket.vat;
        copy.condition = ticket.condition;

        copy.nameTranslations = [];
        for(var i = 0; i < ticket.nameTranslations.length; i++) {
            copy.nameTranslations.push(Translation.createCopy(ticket.nameTranslations[i]));
        }

        copy.conditionTranslations = [];
        for(i = 0; i < ticket.conditionTranslations.length; i++) {
            copy.conditionTranslations.push(Translation.createCopy(ticket.conditionTranslations[i]));
        }

        return copy;
    };

    Ticket.fromApiJson = function(jsonObject) {
        // TODO
        return new Ticket(jsonObject.Id,
            jsonObject.Name);
    };


    return Ticket;
});