/**
 * Created by Jivea
 * +
 */

/* global define */
define(["models/movieFullDescription"], function(MovieDescription) {
    "use strict";

    function Booking(id, movieTitle, startDate, endDate, distributorPercentage, descriptions) {
        this.id = id || null;
        this.movieTitle = movieTitle || '';
        this.startDate = startDate || null;
        this.endDate = endDate || null;
        this.distributorPercentage = distributorPercentage || 0;
        this.descriptions = descriptions || {};
    };

    Booking.createCopy = function(booking) {
        var copy = new Booking(booking.id,
            booking.movieTitle,
            booking.startDate,
            booking.endDate,
            booking.distributorPercentage,
            {});

        for (var key in booking.descriptions) {
            copy.descriptions[key] = MovieDescription.createCopy(booking.descriptions[key]);
        }

        return copy;
    };

    return Booking;
});