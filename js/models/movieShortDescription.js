/**
 * Created by Jivea
 * +
 */

/* global define */
define([] , function () {
    "use strict";

    return function (identifier, originalTitle, lang, title, year) {
        this.identifier = identifier;
        this.title = title;
        this.lang = lang;
        this.originalTitle = originalTitle;
        this.productionYear = year;
    };
});