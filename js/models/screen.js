/**
 * Created by Jivea
 * +
 */

/* global define */
define([], function() {
    "use strict";

    function Screen(id, name, seatCount, displayRank) {
        this.id = id;
        this.name = name;
        this.seatCount = seatCount;
        this.displayRank = displayRank;
    }

    Screen.createCopy = function(screen) {
        var copy = new Screen(screen.id,
            screen.name,
            screen.seatCount,
            screen.displayRank);
        return copy;
    };

    Screen.fromApiJson = function(jsonObject) {
        return new Screen(jsonObject.Id,
            jsonObject.Name,
            jsonObject.SeatCount,
            jsonObject.DisplayRank);
    };

    return Screen;
});