/**
 * Created by Jivea
 * +
 */

/* global define */
define([], function() {
    "use strict";

    function Tax(id, name, value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    Tax.createCopy = function(tax) {
        var copy = new Tax();
        copy.id = tax.id;
        copy.name = tax.name;
        copy.value = tax.value;
        return copy;
    };

    Tax.fromApiJson = function(jsonObject) {
        return new Tax(jsonObject.Id,
            jsonObject.Name,
            jsonObject.Value);
    };

    return Tax;
});