/**
 * Created by Jivea
 * app.js
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var $ = require("jquery");
    var _waitAnimation = require("waitAnim");
    var _toastManager = require("toast");
    var _errorMessageExtractor = require("errorMessageExtractor");
    var _events = require("events");

    exports.init = function() {
        navigate("home");
    };

    exports.events = _events;
    exports.navigate = navigate;


    exports.enableInputs = function(enabled) {
        if(enabled) {
            $(":input").removeAttr("disabled");
        }
        else {
            $(":input").attr("disabled", "disabled");
        }
    };
    exports.onRegistrationCompleted = function() {
        navigate("home");
        displayMessage("Registration completed!", "Welcome to cineven!");
    };
    exports.onWaitRequired = function() {
        _waitAnimation.start();
    };
    exports.onRequiredWaitEnded = function() {
        _waitAnimation.stop();
    };
    exports.onMessage = displayMessage;
    exports.onMessageError = displayErrorMessage;
    exports.onError = displayError;
    exports.onSuccess = displaySuccess;

    function navigate(destination) {
        console.log("navigating to: " + destination);

        var sessionHelper = require("sessionHelperX");
        var nextContent;
        var currentPageModule;
        var goingToHome = false;

        if(!sessionHelper.isUserLoggedIn()) {
            if(destination === 'signup') {
                nextContent = require("hbs!templates/signup");
                currentPageModule = require("signup");
            } else {
                console.log("no session retrieved -- always falling back to  signin page");
                nextContent = require("hbs!templates/signin");
                currentPageModule = require("signin");
            }
        }
        else {
            if(destination == 'signout') {
                nextContent = require("hbs!templates/signout");
                currentPageModule = require("signout");
            } else {
                goingToHome = true;
                nextContent = require("hbs!templates/home");
                currentPageModule = require("home");
            }
        }

        var mainPlaceHolder = $("#content");
        mainPlaceHolder.fadeOut(10, function() {

            mainPlaceHolder.empty();
            mainPlaceHolder.append(nextContent);
            currentPageModule.init();

            mainPlaceHolder.fadeIn("slow");
        });
    }

    function displayMessage(title, message) {
        _toastManager.showInfo(title, message);
    }

    function displaySuccess(message) {
        _toastManager.showSuccess("Opération réussie", message);
    }

    function displayErrorMessage(title, message) {
        _toastManager.showError(title, message);
    }

    function displayError(error) {
        console.log(error);

        var title = "An error occurred";
        var message = error;

        if(typeof(error) == "object") {
            message = _errorMessageExtractor.getMessage(error);
        }
        displayErrorMessage(title, message);
    }
});