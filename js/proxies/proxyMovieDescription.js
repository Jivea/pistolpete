/**
 * Created by Jivea
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var _sessionHelper = require("sessionHelperX");
    var _modelMovieShortDescription = require("models/movieShortDescription");
    var MovieFullDescription = require("models/movieFullDescription");

    exports.getMovieSourceItems = function(lang, searchText, onSuccess, onError) {
        var token = _sessionHelper.getSessionToken();
        if(!token) {
            onError();
        }
        else {
            var headers = {
                Authorization: 'Bearer ' + token
            };

            $.ajax({
                type:    'GET',
                url:     '../api/MovieDescriptionProvider?lang=' + lang + '&searchText=' + encodeURIComponent(searchText),
                headers: headers
            }).done(function(data) {
                    if(onSuccess) {
                        var descriptions = [];
                        for(var i = 0; i < data.length; i++) {
                            descriptions.push(new _modelMovieShortDescription(
                                data[i].Identifier,
                                data[i].OriginalTitle,
                                data[i].Lang,
                                data[i].Title,
                                data[i].ProductionYear));
                        }
                        onSuccess(descriptions);
                    }
                }
            ).fail(function(error) {
                    if(onError) {
                        onError(error);
                    }
                });
        }
    };

    exports.getMovieSourceItemInfo = function(lang, filmId, onSuccess, onError) {
        var token = _sessionHelper.getSessionToken();
        if(!token) {
            onError();
        }
        else {
            var headers = {
                Authorization: 'Bearer ' + token
            };

            $.ajax({
                type:    'GET',
                url:     '../api/MovieDescriptionProvider?lang=' + lang + '&movieId=' + filmId,
                headers: headers
            }).done(function(data) {
                    if(onSuccess) {
                        var fullDescription = MovieFullDescription.create(
                            data.Identifier,
                            data.OriginalTitle,
                            data.Lang,
                            data.Title,
                            data.ReleaseDate,
                            data.Resume,
                            data.Synopsis,
                            data.Duration,
                            data.Distributor,
                            data.Casting,
                            data.Director);

                        onSuccess(fullDescription);
                    }
                }
            ).fail(function(error) {
                    if(onError) {
                        onError(error);
                    }
                });
        }
    };
})
;