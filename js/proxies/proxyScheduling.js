/**
 * Created by Jivea
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";
    var Scheduling = require("models/scheduling");

    var _utilities = require("utilities");
    var _formatter = require("formatter");

    var _sessionHelper = require("sessionHelperX");
    var moment = require("moment");

    exports.getList = function(date, onSuccess, onError) {

        var token = _sessionHelper.getSessionToken();
        if(!token) {
            onError();
        }
        else {
            var headers = {
                Authorization: 'Bearer ' + token
            };

            $.ajax({
                type:    'GET',
                url:     '../api/Scheduling/?date=' + date.format("YYYYMMDD"),
                //url:     '../api/Scheduling',
                headers: headers
            }).done(function(data) {
                var schedules = [];
                for(var i = 0; i < data.length; i++) {
                    schedules.push(Scheduling.fromApiJson(data[i]));
                }
                if(onSuccess) {
                    onSuccess(schedules);
                }
            }).fail(function(error) {
                if(onError) {
                    onError(error);
                }
            });
        }
    };

    exports.deleteSchedule = function(scheduleToDelete, onSuccess, onError) {
        var token = _sessionHelper.getSessionToken();
        if(!token) {
            onError();
        }
        else {
            var headers = {
                Authorization: 'Bearer ' + token
            };

            $.ajax({
                type:    'DELETE',
                url:     '../api/Scheduling/' + scheduleToDelete.id,
                headers: headers
            }).done(function(data) {
                if(onSuccess) {
                    onSuccess();
                }
            }).fail(function(error) {
                if(onError) {
                    onError(error);
                }
            });
        }
    };

    exports.saveSchedules = function(schedulesToSave, onSuccess, onError) {
        var token = _sessionHelper.getSessionToken();
        if(!token) {
            onError();
        }
        else {
            var headers = {
                Authorization: 'Bearer ' + token
            };

            for(var i = 0; i < schedulesToSave.length; i++) {
                schedulesToSave[i].scheduleTime = schedulesToSave[i].scheduleTime.toDate();
            }

            $.ajax({
                type:        'POST',
                url:         '../api/Scheduling/Save2',
                headers:     headers,
                data:        JSON.stringify(schedulesToSave),
                contentType: 'application/json'
            }).done(function(data) {
                if(onSuccess) {
                    onSuccess(Scheduling.fromApiJson(data));
                }
            }).fail(function(error) {
                if(onError) {
                    onError(error);
                }
            });
        }
    };
});