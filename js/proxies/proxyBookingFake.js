/**
 * Created by Jivea
 * +
 */

/* global define */
define(["models/booking", "models/movieFullDescription", "proxies/proxyEditionFake", "utilities"],
    function(Booking, MovieDescription,  ProxyEditionFake, _utilities) {
        "use strict";
        var instance = null;

        function ProxyBookingFake () {
            if(instance !== null) {
                throw new Error("Cannot instantiate more than one ProxyBookingFake.");
            }
            ProxyEditionFake.call(this);
        }


        ProxyBookingFake.prototype = Object.create(ProxyEditionFake.prototype);
        ProxyBookingFake.prototype.constructor = ProxyBookingFake;

        ProxyBookingFake.prototype.getDefaultItems = function() {
            var descriptionFr = MovieDescription.create(
                1,
                "Who framed Roger?",
                "fr",
                "Qui veut la peau de Roger Rabbit ?",
                "date",
                "Roger est un mec sympa",
                "SyRoger est un mec sympa mais un peu lourdingue.",
                "97",
                "Acme",
                "Roger Rabbit, Jessica Rabbit, Vaillant, ...",
                "Robert Zemeckis");

            var descriptionNl = MovieDescription.create(
                1,
                "Who framed Roger?",
                "fr",
                "Roger Rabbit NL",
                "date",
                "Roger Rabbit NL resume",
                "Roger Rabbit NL synopsis",
                "97",
                "Acme",
                "Roger Rabbit, Jessica Rabbit, Vaillant, ...",
                "Robert Zemeckis");

            return [
                new Booking(_utilities.generateGuid(),
                    "Qui veut la peau de Roger Rabbit? Mon gars?",
                    "2015-02-01T00:00:00",//_utilities.trunkDateTimeToDate(new Date()),
                    "2015-04-01T00:00:00",//_utilities.trunkDateTimeToDate(new Date()),
                    4.5,
                    {fr: descriptionFr, nl: descriptionNl}),
                new Booking(_utilities.generateGuid(),
                    "Coralie",
                    "2015-02-01T00:00:00",
                    "2015-03-01T00:00:00",
                    4.5,
                    {}
                ),
                new Booking(_utilities.generateGuid(),
                    "La reine des neiches",
                    "2015-02-01T00:00:00",
                    "2015-06-01T00:00:00",
                    5,
                    {})
            ];
        };

        ProxyBookingFake.getInstance = function() {
            if(instance === null) {
                instance = new ProxyBookingFake();
            }
            return instance;
        };

        return ProxyBookingFake.getInstance();
    });