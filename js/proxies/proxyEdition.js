/**
 * Created by Jivea
 * editionProxy.js
 * +
 */

/* global define */
define(["jquery", "sessionHelperX"],
    function($, _sessionHelper) {
        "use strict";

        function ProxyEdition(baseUrl) {
            this.baseUrl = baseUrl;
        }

        ProxyEdition.prototype.getList = function(onSuccess, onError) {
            var self = this,
                token = _sessionHelper.getSessionToken();
            if(!token) {
                onError();
            }
            else {
                var headers = {
                    Authorization: 'Bearer ' + token
                };

                $.ajax({
                    type:    'GET',
                    url:     self.baseUrl,
                    headers: headers
                }).done(function(data) {
                    var items = [];
                    for(var i = 0; i < data.length; i++) {
                        items.push(self.buildItemFromJsonObject(data[i]));
                    }

                    if(onSuccess) {
                        onSuccess(items);
                    }
                }).fail(function(error) {
                    if(onError) {
                        onError(error);
                    }
                });
            }
        };

        ProxyEdition.prototype.saveItem = function(itemToSave, onSuccess, onError) {
            var self = this,
                token = _sessionHelper.getSessionToken();
            if(!token) {
                onError();
            }
            else {
                var headers = {
                    Authorization: 'Bearer ' + token
                };

                self.onSaving(itemToSave);

                $.ajax({
                    type:        'POST',
                    url:         baseUrl + '/Save',
                    headers:     headers,
                    data:        JSON.stringify(itemToSave),
                    contentType: 'application/json'
                }).done(function(data) {
                    if(onSuccess) {
                        onSuccess(self.buildItemFromJsonObject(data));
                    }
                }).fail(function(error) {
                    if(onError) {
                        onError(error);
                    }
                });
            }
        };

        ProxyEdition.prototype.deleteItem = function(itemToDelete, onSuccess, onError) {

            var token = _sessionHelper.getSessionToken();
            if(!token) {
                onError();
            }
            else {
                var headers = {
                    Authorization: 'Bearer ' + token
                };

                $.ajax({
                    type:    'DELETE',
                    url:     baseUrl + '/'+ itemToDelete.id,
                    headers: headers
                }).done(function(data) {
                    if(onSuccess) {
                        onSuccess();
                    }
                }).fail(function(error) {
                    if(onError) {
                        onError(error);
                    }
                });
            }
        };

        ProxyEdition.prototype.onSaving = function (data) {

        };
        ProxyEdition.prototype.buildItemFromJsonObject = function(data) {
            throw new Error("Function buildItemFromJsonObject must be overridden by child");
        };

        return ProxyEdition;

    });
