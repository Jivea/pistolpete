/**
 * Created by Jivea
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var Tax = require("models/tax");
    var _utilities = require("utilities");
    var _items;

    exports.getList = function(onSuccess, onError) {

        if(!_items) {
            _items = buildDefaultFakeData();
        }
        onSuccess(_items);
    };

    exports.saveItem = function(itemToSave, onSuccess, onError) {
        if(itemToSave.id) {
            var idx = getItemIndex(itemToSave);
            _items[idx] = itemToSave;
        }

        if(onSuccess)
            onSuccess(itemToSave);
    };

    function getItemIndex(item) {
        for(var i = 0; i < _items.length; i++) {
            if(_items[i].id == item.id) {
                return i;
            }
        }
        return -1;
    }

    function buildDefaultFakeData(done) {
        return [
            new Tax(_utilities.generateGuid(),
                "SABAM",
                "7%"),
            new Tax(_utilities.generateGuid(),
                "Taxe communale",
                "5%")
        ];
    }
});