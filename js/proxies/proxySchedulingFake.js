/**
 * Created by Jivea
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";
    var _utilities = require("utilities");

    var Scheduling = require("models/scheduling");
    var bookingProxy = require("proxies/proxyBookingFake");
    var screensProxy = require("proxies/proxyConfigScreenFake");
    var moment = require("moment");
    var _schedules;

    exports.getList = function(date, onSuccess, onError) {

        if(!_schedules) {
            buildDefaultFakeData(function(items) {
                _schedules = items;

                filterSchedules(date, onSuccess);
            });
        } else {
            filterSchedules(date, onSuccess);
        }
    };

    exports.deleteSchedule = function(scheduleToDelete, onSuccess, onError) {
        var scheduleIndex = getScheduleIndex(scheduleToDelete);
        _schedules.splice(scheduleIndex, 1);

        onSuccess();
    };

    exports.saveSchedules = function(schedulesToSave, onSuccess, onError) {
        for(var i = 0; i < schedulesToSave.length; i++) {
            var scheduleToSave = schedulesToSave[i];
            if(scheduleToSave.id) {
                var idx = getScheduleIndex(scheduleToSave);
                _schedules[idx] = scheduleToSave;
            } else {
                scheduleToSave.id = _utilities.generateGuid();
                _schedules.push(scheduleToSave);
            }
        }
        onSuccess(schedulesToSave);
    };

    function filterSchedules(date, done) {

        if(done) {
            var schedulesOfDate = _schedules.filter(function(schedule) {
                var thresholdDate = moment(date).hours(5).minutes(0).seconds(0).milliseconds(0);
                return schedule.scheduleTime.isAfter(thresholdDate) && schedule.scheduleTime.isBefore(thresholdDate.add(1, "d"));
            });

            done(schedulesOfDate);
        }
    }

    function getScheduleIndex(schedule) {
        for(var i = 0; i < _schedules.length; i++) {
            if(_schedules[i].id == schedule.id) {
                return i;
            }
        }
        return -1;
    }

    function buildDefaultFakeData(done) {
        bookingProxy.getList(function(bookings) {
            screensProxy.getList(function(screens) {
                done([
                    new Scheduling(_utilities.generateGuid(),
                        bookings[0].id,
                        bookings[0].movieTitle,
                        screens[0].id,
                        moment().hours(13).minutes(0).seconds(0).millisecond(0)),
                    new Scheduling(_utilities.generateGuid(),
                        bookings[0].id,
                        bookings[0].movieTitle,
                        screens[0].id,
                        moment().hours(15).minutes(15).seconds(0).millisecond(0)),
                    new Scheduling(_utilities.generateGuid(),
                        bookings[0].id,
                        bookings[0].movieTitle,
                        screens[0].id,
                        moment().hours(18).minutes(30).seconds(0).millisecond(0)),
                    new Scheduling(_utilities.generateGuid(),
                        bookings[0].id,
                        bookings[0].movieTitle,
                        screens[0].id,
                        moment().hours(21).minutes(0).seconds(0).millisecond(0)),
                    new Scheduling(_utilities.generateGuid(),
                        bookings[0].id,
                        bookings[0].movieTitle,
                        screens[0].id,
                        moment().hours(23).minutes(30).seconds(0).millisecond(0)),

                    new Scheduling(_utilities.generateGuid(),
                        bookings[1].id,
                        bookings[1].movieTitle,
                        screens[1].id,
                        moment().hours(13).minutes(0).seconds(0).millisecond(0)),

                    new Scheduling(_utilities.generateGuid(),
                        bookings[1].id,
                        bookings[1].movieTitle,
                        screens[1].id,
                        moment().hours(18).minutes(30).seconds(0).millisecond(0)),
                    new Scheduling(_utilities.generateGuid(),
                        bookings[1].id,
                        bookings[1].movieTitle,
                        screens[1].id,
                        moment().hours(21).minutes(0).seconds(0).millisecond(0)),
                    new Scheduling(_utilities.generateGuid(),
                        bookings[1].id,
                        bookings[1].movieTitle,
                        screens[1].id,
                        moment().hours(23).minutes(0).seconds(0).millisecond(0)),
                    new Scheduling(_utilities.generateGuid(),
                        bookings[1].id,
                        bookings[1].movieTitle,
                        screens[3].id,
                        moment().hours(15).minutes(15).seconds(0).millisecond(0)),
                    new Scheduling(_utilities.generateGuid(),
                        bookings[1].id,
                        bookings[1].movieTitle,
                        screens[2].id,
                        moment().hours(15).minutes(0).seconds(0).millisecond(0))
                ]);
            }, null);
        }, null);
    }
});