/**
 * Created by Jivea
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var Ticket = require("models/ticket");
    var Translation = require("models/translation");
    var _utilities = require("utilities");
    var _items;

    exports.getList = function(onSuccess, onError) {

        if(!_items) {
            _items = buildDefaultFakeData();
        }
        onSuccess(_items);
    };

    exports.saveTicket = function(itemToSave, onSuccess, onError) {
        if(itemToSave.id) {
            var idx = getItemIndex(itemToSave);
            _items[idx] = itemToSave;
        } else {
            itemToSave.id = _utilities.generateGuid();
            _items.push(itemToSave);
        }

        if(onSuccess)
            onSuccess(itemToSave);
    };

    exports.deleteTicket = function(itemToDelete, onSuccess, onError) {
        var idx = getItemIndex(itemToDelete);
        _items.splice(idx, 1);

        onSuccess();
    };

    function getItemIndex(item) {
        for(var i = 0; i < _items.length; i++) {
            if(_items[i].id == item.id) {
                return i;
            }
        }
        return -1;
    }

    function buildDefaultFakeData(done) {
        return [
            new Ticket(_utilities.generateGuid(),
                "Standard",
                7,
                21,
                "Normal",
                [
                    new Translation("ticket_name", "fr", "Normal"),
                    new Translation("ticket_name", "en", "Regular"),
                    new Translation("ticket_name", "de", "Standart"),
                    new Translation("ticket_name", "nl", "Normaal")
                ],
                [
                    new Translation("ticket_condition", "fr", "Moins de 12 ans"),
                    new Translation("ticket_condition", "en", "Aged under 12"),
                    new Translation("ticket_condition", "de", "unter 12"),
                    new Translation("ticket_condition", "nl", "onder 12")
                ]
            ),
            new Ticket(_utilities.generateGuid(),
                "Social",
                5.5,
                21,
                "Justificatif",
                [
                    new Translation("ticket_name", "fr", "Social"),
                    new Translation("ticket_name", "en", "Social"),
                    new Translation("ticket_name", "de", "Social"),
                    new Translation("ticket_name", "nl", "Social")
                ],
                [
                    new Translation("ticket_condition", "fr", "Soumis à justification"),
                    new Translation("ticket_condition", "en", "Need to be justified"),
                    new Translation("ticket_condition", "de", "unter 12"),
                    new Translation("ticket_condition", "nl", "onder 12")
                ]
            ),
            new Ticket(_utilities.generateGuid(),
                "Senior",
                5.5,
                21,
                "> 65 ans",
                [
                    new Translation("ticket_name", "fr", "Sénior"),
                    new Translation("ticket_name", "en", "Senior"),
                    new Translation("ticket_name", "de", "Senior"),
                    new Translation("ticket_name", "nl", "Senior")
                ],
                [
                    new Translation("ticket_condition", "fr", "Plus de 65 ans"),
                    new Translation("ticket_condition", "en", "Older than 65"),
                    new Translation("ticket_condition", "de", "Über 65 Jahre"),
                    new Translation("ticket_condition", "nl", "Meer dan 65 jaar")
                ]
            ),
            new Ticket(_utilities.generateGuid(),
                "Etudiant",
                6,
                21,
                "< 18 ans",
                [
                    new Translation("ticket_name", "fr", "Étudiant"),
                    new Translation("ticket_name", "en", "Student"),
                    new Translation("ticket_name", "de", "Student"),
                    new Translation("ticket_name", "nl", "Student")
                ],
                [
                    new Translation("ticket_condition", "fr", "Moins de 18 ans"),
                    new Translation("ticket_condition", "en", "Aged under 18"),
                    new Translation("ticket_condition", "de", "unter 18"),
                    new Translation("ticket_condition", "nl", "onder 18")
                ]
            ),
            new Ticket(_utilities.generateGuid(),
                "Enfant",
                5,
                21,
                "< 12 ans",
                [
                    new Translation("ticket_name", "fr", "Enfant"),
                    new Translation("ticket_name", "en", "Child"),
                    new Translation("ticket_name", "de", "Kind"),
                    new Translation("ticket_name", "nl", "Kind")
                ],
                [
                    new Translation("ticket_condition", "fr", "Moins 12 18 ans"),
                    new Translation("ticket_condition", "en", "Aged under 12"),
                    new Translation("ticket_condition", "de", "unter 12"),
                    new Translation("ticket_condition", "nl", "onder 12")
                ]
            )];
    }
});