/**
 * Created by Jivea
 * proxyBooking.js
 * +
 */

/* global define */
define(["jquery", "moment", "formatter", "models/booking", "models/movieFullDescription", "utilities"],
    function($, moment, formatter, Booking, MovieDescription,  _utilities) {
        "use strict";

        function ProxyBooking() {
            ProxyEdition.call(this, "../api/Booking");
        }

        ProxyBooking.prototype = Object.create(ProxyEdition.prototype);
        ProxyBooking.prototype.constructor = ProxyBooking;

        ProxyBooking.prototype.onSaving = function(booking) {
            booking.startDate = moment(booking.startDate).format("DD-MM-YYYY");
            booking.endDate = moment(booking.endDate).format("DD-MM-YYYY");
        };
        ProxyBooking.prototype.buildItemFromJsonObject = function(data) {
            var descriptions = {};
            if(data.Descriptions) {
                for(var i = 0; i < data.Descriptions.length; i++) {
                    var desc = data.Descriptions[i];
                    descriptions[desc.Lang] = MovieDescription.create(
                        desc.identifier,
                        desc.OriginalTitle,
                        desc.Lang,
                        desc.Title,
                        desc.ReleaseDate,
                        desc.Resume,
                        desc.Synopsis,
                        desc.Duration,
                        desc.Distributor,
                        desc.Casting,
                        desc.Director
                    );
                }
            }

            return new Booking(data.Id,
                data.MovieTitle,
                moment(data.StartDate).toDate(),
                moment(data.EndDate).toDate(),
                data.DistributorPercentage,
                descriptions);
        }

        return ProxyBooking;
    });
