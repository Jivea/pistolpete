/**
 * Created by Jivea
 * +
 */

/* global define */
define([$, "models/screen", "sessionHelperX", "proxies/ProxyEdition"],
    function(jquery, Screen, _sessionHelper, ProxyEdition) {
    "use strict";

    function ProxyConfigScreen() {
        ProxyEdition.call(this, "../api/Screen");
    }

    ProxyConfigScreen.prototype = Object.create(ProxyEdition.prototype);
    ProxyConfigScreen.prototype.constructor = ProxyConfigScreen;

    ProxyConfigScreen.prototype.onSaving = function(screen) {

    };

    ProxyConfigScreen.prototype.buildItemFromJsonObject = function(data) {
        return Screen.fromApiJson(data);
    };

    ProxyConfigScreen.prototype.saveItemsOrder = function(itemsToUpdate, onSuccess, onError) {
        var token = _sessionHelper.getSessionToken();
        if(!token) {
            onError();
        }
        else {
            var headers = {
                Authorization: 'Bearer ' + token
            };

            $.ajax({
                type:    'POST',
                url:     '../api/Screen/UpdateRange',
                headers: headers,
                data:    {'': itemsToUpdate}
            }).done(function(data) {
                if(onSuccess) {
                    onSuccess(data);
                }
            }).fail(function(error) {
                if(onError) {
                    onError(error);
                }
            });
        }
    };

    return ProxyConfigScreen;
});

