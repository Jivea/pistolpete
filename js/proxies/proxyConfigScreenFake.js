/**
 * Created by Jivea
 * +
 */

/* global define */
define(["models/screen", "proxies/proxyEditionFake", "utilities"], function(Screen, ProxyEditionFake, utilities) {
   "use strict";
    var instance = null;

    function ProxyConfigScreenFake() {
        if(instance !== null) {
            throw new Error("Cannot instanciate more than one ProxyConfigScreenFake.");
        }
        ProxyEditionFake.call(this);
    }
    ProxyConfigScreenFake.prototype = Object.create(ProxyEditionFake.prototype);
    ProxyConfigScreenFake.prototype.constructor = ProxyConfigScreenFake;

    ProxyConfigScreenFake.getInstance = function() {
        if(instance === null){
            instance = new ProxyConfigScreenFake();
        }
        return instance;
    };

    ProxyConfigScreenFake.prototype.getDefaultItems = function() {
        return [
            new Screen("333-fds-5",
                "Salle 1",
                220,
                1),
            new Screen(
                "333-fds-6",
                "Salle 2",
                400,
                2),
            new Screen("333-fds-7",
                "Salle 3",
                157,
                3),
            new Screen("333-fds-8",
                "Salle 4",
                444,
                4)
        ];
    };

    ProxyConfigScreenFake.prototype.saveItemsOrder = function(itemsToUpdate, onSuccess, onError) {
        if(itemsToUpdate) {
            for(var i = 0; i < itemsToUpdate.length; i++) {
                var screenIdx = itemsToUpdate.getIndexWithId(itemsToUpdate[i].id);
                if(screenIdx > -1) {
                    this.items[screenIdx].displayRank = itemsToUpdate[i].displayRank;
                }
            }
        }

        if(onSuccess)
            onSuccess("success");
    };

    return ProxyConfigScreenFake.getInstance();
});



define(function(require, exports, module) {
    "use strict";

    exports.saveItemsOrder = function(itemsToUpdate, onSuccess, onError) {
        if(itemsToUpdate) {
            for(var i = 0; i < itemsToUpdate.length; i++) {
                var screenIdx = getScreenIndex(itemsToUpdate[i]);
                if(screenIdx > -1) {
                    _screens[screenIdx].displayRank = itemsToUpdate[i].displayRank;
                }
            }
        }

        if(onSuccess)
            onSuccess("success");
    };


});