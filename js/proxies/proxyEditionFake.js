/**
 * Created by Jivea
 */

/* global define */
define(["utilities"],
    function(_utilities) {
        "use strict";

        function ProxyEditionFake() {
            this.items = this.getDefaultItems();
        }

        ProxyEditionFake.prototype.getList = function(onSuccess, onError) {
            onSuccess(this.items);
        };

        ProxyEditionFake.prototype.saveItem = function(toSave, onSuccess, onError) {
            var self = this;
            if(toSave.id) {
                var idx = self.items.getIndexWithId(toSave.id);
                self.items[idx] = toSave;
            } else {
                toSave.id = _utilities.generateGuid();
                self.items.push(toSave);
            }
            onSuccess(toSave);
        };

        ProxyEditionFake.prototype.deleteItem = function(toDelete, onSuccess, onError) {
            var self = this;
            self.items.splice(self.items.getIndexWithId(toDelete.id), 1);
            onSuccess();
        };

        ProxyEditionFake.prototype.getDefaultItems = function() {
            throw new Error("Must be overridden by child.");
        };

        return ProxyEditionFake;

    });

