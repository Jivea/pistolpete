/**
 * Created by Jivea
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var MovieShortDescription = require("models/movieShortDescription");
    var MovieFullDescription = require("models/movieFullDescription");

    exports.getMovieSourceItems = function(lang, searchText, onSuccess, onError) {
        if(onSuccess) {
            var descriptions = [];

            descriptions.push(new MovieShortDescription(
                "11",
                searchText + "-orig",
                lang,
                searchText,
                "1985"));

            descriptions.push(new MovieShortDescription(
                    "1",
                    "Back to the Future",
                    lang,
                    "Retour vers le futur",
                    "1985"));

            descriptions.push(new MovieShortDescription(
                "2",
                "100 ans Universal - Trilogie Retour vers le futur",
                lang,
                "100 ans Universal - Trilogie Retour vers le futur",
                "1980"));

            descriptions.push(new MovieShortDescription(
                "3",
                "Back to the Future Part III",
                lang,
                "Retour vers le futur III",
                "1990"));

            descriptions.push(new MovieShortDescription(
                "3",
                "Back to the Future Part II",
                lang,
                "Retour vers le futur II",
                "1989"));

            onSuccess(descriptions);
        }
    };

    exports.getMovieSourceItemInfo = function(lang, filmId, onSuccess, onError) {

        if(onSuccess) {
            var fullDescription = MovieFullDescription.create(
                1,
                "OriginalTitle",
                "fr",
                "Title",
                "ReleaseDate",
                "Resume",
                "Synopsis",
                "Duration",
                "Distributor",
                "Casting",
                "Director");

            onSuccess(fullDescription);
        }
    };
})
;