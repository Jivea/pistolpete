/**
 * Created by Jivea
 * +
 */


/* global define */

define(function () {
    "use strict";

    return {
        ADD: "add",
        SAVE: "save",
        DELETE: "delete",
        CANCEL: "cancel",
        UP: "up",
        DOWN: "down",

        properties: {
            add: {text: "Ajouter", icon: "plus"},
            save: {text: "Sauver", icon: "check"},
            delete: {text: "Effacer", icon: "trash"},
            cancel: {text: "Annuler", icon: "undo"},
            up: {text: "Monter", icon: "arrow-up"},
            down: {text: "Descendre", icon: "arrow-down"}
        }
    }
});