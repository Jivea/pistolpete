/**
 * Created by Jivea
 * bladeManager.js
 * + Handle blades manipulations
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var $ = require("jquery");
    var _converter = require("converter");

    var _bladeDefinitions = require("blades/bladeDefinitions");
    var _bladeTemplate = require("hbs!templates/blade");
    var _openedBlades;

    exports.init = function() {
        _openedBlades = [];
    }
    exports.showBlade = showBlade;
    exports.closeBlade = closeBlade;

    function showBlade(container, bladeName, dataContext) {
        // do nothing if blade is already opened or not defined
        if(_openedBlades.indexOf(bladeName) > -1
            || _bladeDefinitions.isBladeDefined(bladeName) == false) {
            return;
        }

        // if first-level blade, then clear out the area
        if(_bladeDefinitions.isFirstLevelBlade(bladeName)) {
            closeAllBlades(function() {
                openBlade(container, bladeName, dataContext);
            });
        } else {
            openBlade(container, bladeName, dataContext);
        }
    }

    function openBlade(container, bladeName, dataContext) {
        var bladeJs = _bladeDefinitions.getBlade(bladeName);
        var bladeConfig = bladeJs.getConfiguration();
        var bladeContent = _bladeTemplate(bladeConfig);

        container.append(bladeContent);
        var isFirstBlade = _openedBlades.length == 0;
        _openedBlades.push(bladeName);

        setTimeout(function() {
            var blade = $("#blade-" + bladeName);
            blade.toggleClass("opened", true);
            blade.css('width', bladeConfig.width);
            blade.toggleClass("first", isFirstBlade);

            blade.find(".blade-commands").on("click", "li", function(event) {
                var commandName = $(event.currentTarget).attr("data-command-name");
                var command = bladeConfig.getCommand(commandName);
                bladeJs[command.handlerName]();
            });
            bladeJs.init(dataContext);
        }, 100);
    }

    function closeBlade(bladeName, done) {
        if(_openedBlades.length == 0) {
            if(done) done([]);
            return;
        }

        var bladeIndex = _openedBlades.length - 1;  // if no name, remove the last blade
        if(bladeName != null && bladeName != '') {
            bladeIndex = _openedBlades.indexOf(bladeName);
        }

        if(bladeIndex == -1) {
            if(done) done([]);
            return ;
        }

        closeBladesUntilIndex(bladeIndex, done);
    }

    function closeBladesUntilIndex(bladeIndex, done, closedBladeNames) {
        if(closedBladeNames == null) {
            closedBladeNames = [];
        }

        if(_openedBlades.length > bladeIndex) {
            closeLastBlade(function(closedBladeName) {
                closedBladeNames.push(closedBladeName);
                closeBladesUntilIndex(bladeIndex, done, closedBladeNames);
            });
        } else {
            if(done) done(closedBladeNames);
        }
    }

    function closeLastBlade(done) {

        var bladeIndex = _openedBlades.length - 1;
        var bladeName = _openedBlades[bladeIndex];
        var blade = _bladeDefinitions.getBlade(bladeName);

        _openedBlades.splice(bladeIndex, 1);

        var bladeToClose = $("#blade-" + bladeName);
        bladeToClose.toggleClass("opened", false);
        bladeToClose.css('width', '0px');

        if(blade.unInit)
            blade.unInit();

        // Just wait the end of the transition effect before actually removing the blade
        var transitionDuration = _converter.cssTimeToMilliseconds(bladeToClose.css("transition-duration"));
        setTimeout(function() {
            bladeToClose.remove();

            if(done) done(bladeName);
        }, transitionDuration);
    }

    function closeAllBlades(done) {
        if(_openedBlades.length > 0) {
            closeLastBlade(function() {
                closeAllBlades(done);
            });
        } else {
            if(done) done();
        }
    }
});