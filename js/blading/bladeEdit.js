/**
 * Created by Jivea
 * booking.js
 * +
 */

/* global define */
define(["jquery", "../app", "home", "blading/blade", "utilities"], function($, _app, _home, Blade, _utilities) {
    "use strict";

    function BladeEdit(bladeConfiguration, template, newItem) {
        var self = this;
        this.isNewItem = newItem;
        this.originalItem = null;
        this.editedItem = null;
        this.isInitDone = false;

        Blade.call(this, bladeConfiguration, template);

        this.initItemEdition = function() {
            var modelType = self.getModelType();
            self.editedItem = self.isNewItem || (!self.originalItem) ? new modelType() : modelType.createCopy(self.originalItem);
            self.bindForm(self.originalItem);
            self.onEditedItemChanged();
            self.isInitDone = true ;
        };

        this.save = function() {
            Blade.prototype.updateCommandButtonState.call(self, "save", false);

            var editedItem = self.getEditedItem();
            self.getModelProxy().saveItem(editedItem, function(savedItem) {
                if(self.isNewItem) {
                    _app.onSuccess("Nouvel élement sauvé.");
                    _app.events.publish(self.getEventPrefix()+"-saved", savedItem);
                } else {
                    _app.onSuccess("Modification enregistrée.");
                    _app.events.publish(self.getEventPrefix()+"-updated", savedItem);
                }
            }, function(error) {
                _app.onError(error);
            });
        };
        this.delete = function() {
            self.getModelProxy().deleteItem(self.originalItem, function() {
                _app.onSuccess("Élément effacé");
                _app.events.publish(self.getEventPrefix()+"-deleted");
            }, function(error) {
                _app.onError(error);
            });
        };
        this.cancel = function() {
            self.initItemEdition();
        };
    }

    BladeEdit.prototype = Object.create(Blade.prototype);
    BladeEdit.prototype.constructor = BladeEdit;
    BladeEdit.prototype.getModelType = function() {
        throw new Error("Function getModelType must be overridden by child");
    };
    BladeEdit.prototype.getModelProxy = function() {
        throw new Error("Function getModelProxy must be overridden by child");
    };

    BladeEdit.prototype.init = function(dataContext) {
        dataContext = dataContext || { item : new (this.getModelType())()};
        this.originalItem = dataContext.item;
        this.editedItem = null;
        this.isInitDone = false;

        Blade.prototype.init.call(this);

        this.initItemEdition();
    };

    BladeEdit.prototype.unInit = function() {
        Blade.prototype.unInit.call(this);
    };

    BladeEdit.prototype.bindForm = function(item) {
        throw new Error("Function bindForm must be overridden by child");
    };

    BladeEdit.prototype.onEditedItemChanged = function () {
        var self = this;
        var editedItem = this.getEditedItem();
        var hasChanged = !_utilities.areArraysEqual(this.originalItem, editedItem);
        Blade.prototype.updateCommandButtonState.call(this, ["save", "cancel"], hasChanged);

        if(self.isInitDone && hasChanged) {
            _app.events.publish(self.getEventPrefix()+"-changed", editedItem);
        }
    };

    BladeEdit.prototype.getEditedItem = function () {
        throw new Error("Function bindForm must be overridden by child");
    };

    BladeEdit.prototype.getEventPrefix = function() {
        return this.getModelType().name.toLowerCase();
    };

    BladeEdit.prototype.subscribeDOMEvents = function() {
        throw new Error("Function subscribeDOMEvents must be overridden by child");
    };

    BladeEdit.prototype.onCommandSave = function() {
        this.save();
    };
    BladeEdit.prototype.onCommandDelete = function() {
        this.delete();
    };
    BladeEdit.prototype.onCommandCancel = function() {
        this.cancel();
    };

    return BladeEdit;
});
