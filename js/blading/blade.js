/**
 * Created by Jivea
  * +
 */

/* global define */
define(["jquery", "../app", "home"], function($, _app, _home) {
    "use strict";

    function Blade(bladeConfiguration, template) {
        var self = this;
        this.bladeConfiguration = bladeConfiguration;
        this.template = template;
        this.dataContext = null;

        this.hookCloseBtn = function(bladeName) {
            var btn = $("#blade-" + bladeName).find(".blade-closebtn");
            btn.on("click", function() {
                _app.events.publish("blade-closebtn-clicked", bladeName);
            });
        };

        this.renderBladeTitle = function() {
            if(self.bladeConfiguration.title) {
                var headerLabel = $("#blade-" + self.bladeConfiguration.name).find(".blade-header-text");
                headerLabel.text(self.bladeConfiguration.title);
            }
        };

        this.subscribeAppEvents = function () {
            var appEvents = self.getAppEventsToSubscribe();
            for(var i = 0; i < appEvents.length; i++) {
                var event = appEvents[i];
                _app.events.subscribe(event.name, event.handler);
            }
        };

        this.unSubscribeAppEvents = function () {
            var appEvents = self.getAppEventsToSubscribe();
            for(var i = 0; i < appEvents.length; i++) {
                var event = appEvents[i];
                _app.events.unSubscribe(event.name, event.handler);
            }
        };

        this.onBladeClosed = function(bladeName) {
        };
    }

    Blade.prototype.init = function () {
        this.hookCloseBtn(this.bladeConfiguration.name);
        this.subscribeAppEvents();

        this.renderBladeTitle();
        this.renderBlade();
        this.subscribeDOMEvents();
    };

    Blade.prototype.unInit = function unInit() {
        this.unSubscribeAppEvents();
    };

    Blade.prototype.getAppEventsToSubscribe = function() {
        return [{name: "blade-closed", handler: this.onBladeClosed}];
    };

    Blade.prototype.getConfiguration = function () {
        return this.bladeConfiguration;
    };

    Blade.prototype.getBladeName = function () {
        return this.bladeConfiguration.name;
    };

    Blade.prototype.setBladeTitle = function (bladeTitle) {
        this.bladeConfiguration.title = bladeTitle;
        this.renderBladeTitle();
    };

    Blade.prototype.setDataContext = function(dataContext) {
        this.dataContext = dataContext;
    };

    Blade.prototype.showSpinner = function (visible) {
        var spinner = $("#blade-" + this.bladeName).find(".blade-spinner");
        spinner.toggleClass("hidden", !visible);
    };

    Blade.prototype.renderBlade = function() {
        if(this.template) {
            var container = $("#blade-content-"+this.getBladeName());
            var content = this.template(this.dataContext);

            container.empty();
            container.append(content);
        }
    };

    Blade.prototype.subscribeDOMEvents = function() {
    };

    Blade.prototype.updateCommandButtonState = function(command, enabled) {
        var commands = [].concat(command);
        for (var i = 0; i < commands.length; i++) {
            var cmd = commands[i];
            $("#blade-" + this.getBladeName() + "-commands-"+cmd).toggleClass('disabled', !enabled);
        }
    };

    return Blade;
});