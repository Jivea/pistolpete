/**
 * Created by Jivea
 * +
 */

/* global define */
define(["blading/bladeConfigurationCommand"], function(BladeConfigurationCommand) {
    "use strict";

    function BladeConfiguration(name, title, icon, width, commands) {
        this.name = name;
        this.title = title;
        this.icon = icon;
        this.width = width;
        this.commands = [];
        if(commands) {
            for(var i = 0; i < commands.length; i++) {
                var command = commands[i];
                this.commands.push(new BladeConfigurationCommand(command));
            }
        }
    }

    BladeConfiguration.prototype.getCommand = function(name) {
        for(var i = 0; i < this.commands.length; i++) {
            if(this.commands[i].name == name) {
                return this.commands[i];
            }
        }
    };

    return BladeConfiguration;
});