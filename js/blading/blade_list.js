/**
 * Created by Jivea
 * +
 */

/* global define */
define(["jquery", "../app", "home", "blading/blade"], function($, _app, _home, Blade) {
    "use strict";

    function BladeList(bladeConfiguration, gridTemplate, proxy, containerQuery) {
        var self = this,
            bladeName = bladeConfiguration.name,
            items = [],
            selectedItem = null;

        if(!containerQuery) {
            containerQuery = "#blade-content-" + bladeName;
        }

        Blade.call(this, bladeConfiguration, gridTemplate);

        this.getAndRenderItems = function(done) {
            Blade.prototype.showSpinner.call(self, true);
            proxy.getList(function(retrievedItems) {
                Blade.prototype.showSpinner.call(self, false);
                items = retrievedItems;

                Blade.prototype.setDataContext.call(self, items);
                Blade.prototype.renderBlade.call(self);

                registerToUiEvents(items);

                if(done != null)
                    done();
            }, function(error) {
                _app.onError(error);
            });
        };

        this.onNewItemSaved = function(savedItem) {
            self.getAndRenderItems(function() {
                var bladeToClose = selectedItem == "new" ? bladeName + "_new" : bladeName + "_edit";
                _home.closeBlade(bladeToClose, function() {
                    selectItem(savedItem);
                });
            });
        };
        this.onItemUpdated = function(savedItem) {
            self.getAndRenderItems(function() {
                selectItem(savedItem);
            });
        };
        this.onItemDeleted = function() {
            var bladeToClose = selectedItem == "new" ? bladeName + "_new" : bladeName + "_edit";
            _home.closeBlade(bladeToClose, function() {
                self.getAndRenderItems();
            });
        };
        this.onNewItemCanceled = function() {
            _home.closeBlade(bladeName + "_new");
        };
        this.onNewItemClick = function() {
            if(selectedItem != null) {
                var bladeToClose = selectedItem == "new" ? bladeName + "_new" : bladeName + "_edit";
                _home.closeBlade(bladeToClose, function() {
                    addNewItem();
                });
            } else {
                addNewItem();
            }
        };

        this.moveUp = function() {
            moveItem(selectedItem, -1);
        };

        this.moveDown = function() {
            moveItem(selectedItem, 1);
        };

        this.getLastItem = function() {
            if(!items || items.length == 0) {
                return null;
            } else {
                return items[items.length - 1];
            }
        };

        this.clearSelection = function() {
            this._selectedItem = null;
            clearRowSelection();
        };

        function registerToUiEvents () {
            $(".gridList > tbody > tr").on("click", function() {
                var itemId = $(this).attr("data-item-id");
                var itemObject = items.getObjectWithId(itemId);
                onItemClick(itemObject);
            });

            $(".gridList > thead > tr").hover(function() {
                $(".gridList td:first-child").siblings().toggleClass("columnVisible");
            });
        }

        function onItemClick(item) {
            if(selectedItem != null) {
                var bladeToClose = selectedItem == "new" ? bladeName + "_new" : bladeName + "_edit";
                _home.closeBlade(bladeToClose, function() {
                    selectItem(item);
                });
            } else {
                selectItem(item);
            }
        }

        function addNewItem() {
            selectedItem = "new";
            clearRowSelection();
            _home.openBlade(bladeName + "_new");
        }

        function selectItem(item) {
            selectedItem = item;
            selectItemRow(item.id);
            _home.openBlade(bladeName + "_edit", { item: item });
        }

        function moveItem(itemToMove, moveValue) {
            var self = this;
            if(itemToMove != null && itemToMove.displayRank != undefined) {
                var formerIndex = items.getIndexWithId(itemToMove.id);
                if(formerIndex + moveValue >= 0 && formerIndex + moveValue <= items.length) {
                    changeItemDisplayRank(formerIndex, formerIndex + moveValue, function() {
                        self.getAndRenderItems(function() {
                            selectItem(itemToMove);
                        });
                    });
                }
            }
        }

        function changeItemDisplayRank(formerRank, newRank, done) {
            var toUpdate = [];

            items.splice(formerRank, 1);  // remove
            items.splice(newRank, 0, selectedItem); // insert

            for(var i = 0; i < items.length; i++) {
                if(items[i].displayRank != i) {
                    items[i].displayRank = i;
                    toUpdate.push(items[i]);
                }
            }

            if(toUpdate.length > 0) {
                proxy.saveItemsOrder(toUpdate, function(success) {
                    if(done) done();
                }, function(error) {
                    _app.onError(error);
                });
            }
        }

        function selectItemRow(itemId) {
            var tr = $("[data-item-id='" + itemId + "']");
            tr.toggleClass("gridList-row-selected").siblings().removeClass("gridList-row-selected");
        }

        function clearRowSelection() {
            $(".gridList > tbody > tr").removeClass("gridList-row-selected");
        }
    }

    BladeList.prototype = Object.create(Blade.prototype);
    BladeList.prototype.constructor = BladeList;

    BladeList.prototype.init = function() {
        Blade.prototype.init.call(this);
        this.getAndRenderItems();
    };

    BladeList.prototype.unInit = function() {
        Blade.prototype.unInit.call(this);
    };

    BladeList.prototype.getAppEventsToSubscribe = function() {
        var appEvents = Blade.prototype.getAppEventsToSubscribe.call(this);
        appEvents.push({name:this.getBladeName() + "-saved", handler: this.onNewItemSaved});
        appEvents.push({name:this.getBladeName() + "-updated", handler: this.onItemUpdated});
        appEvents.push({name:this.getBladeName() + "-deleted", handler: this.onItemDeleted});
        appEvents.push({name:this.getBladeName() + "-addcanceled", handler: this.onNewItemCanceled});
        return appEvents;
    };

    BladeList.prototype.onCommandAdd = function() {
        this.onNewItemClick();
    };

    BladeList.prototype.onCommandUp = function() {
        this.moveUp();
    };

    BladeList.prototype.onCommandDown = function() {
        this.moveDown();
    };

    BladeList.prototype.getNextDisplayRank = function() {
        var lastItem = this.getLastItem();
        if(lastItem === null) {
            return 1;
        } else {
            return lastItem.displayRank + 1;
        }
    };
    BladeList.prototype.onBladeClosed = function (bladeName) {
        if(bladeName == this.getBladeName() + "_new" || bladeName == this.getBladeName() + "_edit") {
            this.clearSelection();
        }
    };
    return BladeList;
});