/**
 * Created by Jivea
 * +
 */

/* global define */
define(["blading/bladeConfigurationCommandType"], function(BladeConfigurationCommandType) {
    "use strict";

    function BladeConfigurationCommand(action) {
        this.name = action;
        this.text = getText(action);
        this.icon = getIcon(action);
        this.handlerName = getHandlerName(action);
    }


    function getText(action) {
        return BladeConfigurationCommandType.properties[action].text;
    }

    function getIcon(action) {
        return BladeConfigurationCommandType.properties[action].icon;
    }

    function getHandlerName(action) {
        return "onCommand"+ action.capitalize();
    }

    return BladeConfigurationCommand;
});