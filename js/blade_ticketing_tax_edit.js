/**
 * Created by Jivea
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var Tax = require("models/tax");

    var $ = require("jquery");
    var _app = require("app");
    var _home = require("home");
    var _proxy = require("ticketingTaxProxyX");

    var _utilities = require("utilities");
    var _bladeCommon = require("blade_common");

    var _bladeName;
    var _original,
        _editedItem;


    exports.init = function(dataContext) {
        _original = dataContext;
        _bladeName = "ticketing_configTax_edit";

        _bladeCommon.init(_bladeName);

        initEdition();

    };

    exports.onCommandSave = function() {
        updateCommandButtonsState(false);

        var editedItem = getEditedItem();
        _proxy.saveItem(editedItem, function(savedItem) {
                _app.onSuccess("Taxe modifiée.");
                _app.events.publish("ticketing_configTax-updated", savedItem);
            },
            function(error) {
            _app.onError(error);
        });
    };

    exports.onCommandCancel = function() {
        initEdition();
    };

    function initEdition() {
        _editedItem = Tax.createCopy(_original);
        loadForm();
        updateCommandButtonsState(false);
        bindForm(_editedItem);
    }

    function loadForm() {
        var container = $("#blade-content-" + _bladeName);
        var form = require("hbs!templates/taxForm")();

        container.empty();
        container.append(form);
    }

    function bindForm(item) {

        var txtName = $("#taxForm_name");
        var txtValue = $("#taxForm_value");

        if(item != null) {
            txtName.val(item.name);
            txtValue.val(item.value);
        }

        txtName.on("change paste keyup", onUserInput);
        txtValue.on("change paste keyup", onUserInput);
    }

    function onUserInput() {
        var editedItem = getEditedItem();
        var hasChanged = !_utilities.areArraysEqual(_original, editedItem);
        updateCommandButtonsState(hasChanged);
    }

    function getEditedItem() {
        _editedItem.id = _original.id;
        _editedItem.name = $("#taxForm_name").val();
        _editedItem.value = $("#taxForm_value").val();
        return _editedItem;
    }

    function updateCommandButtonsState(itemHasChanged) {
        $("#blade-" + _bladeName + "-commands-save").toggleClass('disabled', !itemHasChanged);
        $("#blade-" + _bladeName + "-commands-cancel").toggleClass('disabled', !itemHasChanged);
    }
});