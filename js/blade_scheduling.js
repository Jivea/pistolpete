/**
 * Created by Jivea
 * blade_schedule.js
 * +
 */

/* global define */

define(["jquery", "moment", "formatter", "app", "home", "blading/blade", "models/booking", "configScreenProxyX","schedulingProxyX", "utilities"],
    function($, moment, formatter, _app, _home, Blade, Booking, _screensProxy, _schedulingProxy, _utilities) {
    "use strict";

    function BladeScheduling(bladeConfiguration, template) {
        var self = this;
        this.screens = null;
        this.schedules = null;
        this.currentDay = moment().hours(0).minutes(0).seconds(0).milliseconds(0);
        this.selectedSchedule = null;

        Blade.call(this, bladeConfiguration, template);

        this.getAndRenderItems = function() {
            loadScreens(function() {
                getAndRenderSchedules();
            });
        };

        this.registerDOMEvents = function() {
            $("#scheduling_previousDay").on("click", function() {
                self.currentDay.add(-1, "d");
                self.schedules = [];
                renderSchedules(true);
                loadSchedules(function() {
                    renderSchedules();
                });
            });
            $("#scheduling_nextDay").on("click", function() {
                self.currentDay.add(1, "d");
                self.schedules = [];
                renderSchedules(true);
                loadSchedules(function() {
                    renderSchedules();
                });
            });

            $(".scheduleDayView-block").on("click", function() {
                var itemId = $(this).attr("data-item-id");
                var itemObject = self.schedules.getObjectWithId(itemId);
                onScheduleClick(itemObject);
            });
        };

        this.onAddButtonClick = function () {
            closeEdition(function() {
                setScheduleSelection();
                openEdition();
            });
        };

        this.onItemUpdated = function(item) {
            getAndRenderSchedules(function() {
                setScheduleSelection(item);
                openEdition();
            });
        };

        this.onItemAdded = function (item){
            closeEdition(function() {
                setScheduleSelection(item);
                getAndRenderSchedules();
            });
        };
        this.onItemDeleted = function (item) {
            _home.closeBlade(self.getBladeName + "_edit", function() {
                getAndRenderSchedules();
            });
        };

        function loadScreens(done) {
            updateSpinner();
            _screensProxy.getList(function(items) {
                self.screens = items;
                updateSpinner();

                if(done != null)
                    done();
            }, function(error) {
                _app.onError(error);
            });
        }

        function getAndRenderSchedules(callback) {
            loadSchedules(function() {
                renderSchedules();
                if(callback) {
                    callback();
                }
            });
        }

        function loadSchedules(done) {
            self.schedules = null;
            updateSpinner();
            _schedulingProxy.getList(self.currentDay, function(items) {
                self.schedules = items;

                updateSpinner();

                if(done != null)
                    done();
            }, function(error) {
                _app.onError(error);
            });
        }

        function renderSchedules(isLoadingData) {
            Blade.prototype.setDataContext.call(self, {
                isLoadingData: isLoadingData ? isLoadingData : false,
                currentDate: self.currentDay,
                screens:     self.screens,
                timeSlots:   transformSchedulesToTimeSlots()
            });
            Blade.prototype.renderBlade.call(self);

            if(self.selectedSchedule) {
                setScheduleSelection(self.selectedSchedule);
            }

            self.registerDOMEvents();
        }

        function transformSchedulesToTimeSlots() {

            self.schedules.sort(function(a, b) {
                var timeDiff = a.scheduleTime - b.scheduleTime;
                if(timeDiff != 0) {
                    return timeDiff;
                } else {
                    return self.screens.getIndexWithId(a.screenId) - self.screens.getIndexWithId(b.screenId);
                }
            });

            var result = [];
            var currentTimeSlot;
            for(var i = 0; i < self.schedules.length; i++) {
                if(!self.schedules[i].scheduleTime.isSame(currentTimeSlot)) {
                    result.push({schedules: []});

                    for(var j = 0; j < self.screens.length; j++) {
                        result[result.length - 1].schedules.push([]);
                    }
                    currentTimeSlot = self.schedules[i].scheduleTime;
                }

                var scheduleScreenIndex = self.screens.getIndexWithId(self.schedules[i].screenId);
                if(scheduleScreenIndex != -1) {
                    result[result.length - 1].schedules[scheduleScreenIndex].push(self.schedules[i]);
                } else {
                    console.warn("There is a scheduled show for an unknown screen. It will not be displayed");
                }
            }
            return result;
        }

        function setScheduleSelection(item) {
            self.selectedSchedule = item;
            $(".scheduleDayView-block ").removeClass("scheduleDayView-schedule-selected");
            if(self.selectedSchedule) {
                var block = $(".scheduleDayView-block[data-item-id=" + '"' + self.selectedSchedule.id + '"]');
                block.toggleClass("scheduleDayView-schedule-selected");
            }
        }

        function onScheduleClick(item) {
            closeEdition(function() {
                setScheduleSelection(item);
                openEdition();
            });
        }

        function openEdition() {
            _home.openBlade(self.getBladeName() + (self.selectedSchedule ? "_edit" : "_new"), {
                currentDay: self.currentDay,
                item: self.selectedSchedule,
                screens:  self.screens
            });
        }

        function closeEdition(callback) {
            var bladeToClose = self.getBladeName() + (self.selectedSchedule ? "_edit" : "_new");
            _home.closeBlade(bladeToClose, function() {
                if(callback) {
                    callback();
                }
            });
        }

        function updateSpinner() {
            Blade.prototype.showSpinner.call(self.screens == null || self.schedules == null);
        }
    }

    BladeScheduling.prototype = Object.create(Blade.prototype);
    BladeScheduling.prototype.constructor = BladeScheduling;
    BladeScheduling.prototype.init = function(dataContext) {
        Blade.prototype.init.call(this);
        this.getAndRenderItems();
    };

    BladeScheduling.prototype.getAppEventsToSubscribe = function() {
        var appEvents = Blade.prototype.getAppEventsToSubscribe.call(this);
        appEvents.push({ name: this.getBladeName() + "-deleted", handler: this.onItemDeleted});
        appEvents.push({ name: this.getBladeName() + "-added", handler: this.onItemAdded});
        appEvents.push({ name: this.getBladeName() + "-updated", handler: this.onItemUpdated});
        return appEvents;
    };

    BladeScheduling.prototype.subscribeDOMEvents = function() {
        this.registerDOMEvents();
    };

    BladeScheduling.prototype.onCommandAdd = function() {
        this.onAddButtonClick();
    };
    return BladeScheduling;
});
