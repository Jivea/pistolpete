/**
 * Created by Jivea
 * sessionHelper.js
 * + module handling session management
 */

/* global define */
define(function(require, exports, module) {
    "use strict";
    var _tokenKey = "cineven_login_tokenKey";

    exports.isUserLoggedIn = function() {
        var token = sessionStorage.getItem(_tokenKey);
        return token != null;
    };

    exports.getSessionToken = getSessionToken;

    exports.getUserInfo = function(onSuccess, onError) {
        var token = getSessionToken();
        if(!token) {
            onError();
        }
        else {
            var headers = {
                Authorization: 'Bearer ' + token
            };

            $.ajax({
                type:    'GET',
                url:     '../api/Account/UserInfo',
                headers: headers
            }).done(function(data) {
                if(onSuccess) {
                    onSuccess(data);
                }
            }).fail(function(error) {
                if(onError) {
                    onError(error);
                }
            });
        }
    };

    exports.signUp = function(signUpRequest, onSuccess, onError) {
        $.ajax({
            type:        'POST',
            url:         '/api/Account/Register',
            contentType: 'application/json; charset=utf-8',
            data:        JSON.stringify(signUpRequest)
        }).done(function(data) {
            if(onSuccess) {
                onSuccess(data)
            }
        }).fail(function(error) {
            if(onError) {
                onError(error);
            }
        });
    };

    exports.signIn = function(signInRequest, onSuccess, onError) {
        $.ajax({
            type: 'POST',
            url:  '../Token',
            data: signInRequest
        }).done(function(data) {
            sessionStorage.setItem(_tokenKey, data.access_token);
            if(onSuccess) {
                onSuccess(data.access_token)
            }
        }).fail(function(error) {
            if(onError) {
                onError(error);
            }
        });
    };

    exports.signOut = function(onSuccess, onError) {
        var token = getSessionToken();
        if(!token) {
            onError();
        }
        else {
            var headers = {
                Authorization: 'Bearer ' + token
            };

            $.ajax({
                type:    'POST',
                url:     '../api/Account/Logout',
                headers: headers
            }).done(function(data) {
                sessionStorage.removeItem(_tokenKey);
                if(onSuccess) {
                    onSuccess(data);
                }
            }).fail(function(error) {
                if(onError) {
                    onError(error);
                }
            });
        }
    };

    exports.buildSignUpRequest = function(userName,
                                          name,
                                          firstName,
                                          email,
                                          password,
                                          passwordConfirm) {
        return {
            UserName:        userName,
            Name:            name,
            FirstName:       firstName,
            Email:          email,
            Password:        password,
            ConfirmPassword: passwordConfirm
        };
    };

    exports.buildSignInRequest = function(userName, password) {
        return {
            grant_type: 'password',
            username:   userName,
            password:   password
        };
    };

    function getSessionToken() {
        return sessionStorage.getItem(_tokenKey);
    }
});