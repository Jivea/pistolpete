/**
 * Created by Jivea
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var Translation = require("models/translation");

    var $ = require("jquery");
    var _app = require("app");
    var _bladeCommon = require("blade_common");

    var _bladeName = "ticketing_configTicket_trad";
    var _translations ;

    exports.init = function(dataContext) {
        _bladeCommon.init(_bladeName);
        _translations = dataContext;
        initTranslationEdition();
    };

    function initTranslationEdition() {
        loadForm();
        bindForm(_translations);
    }

    function loadForm() {
        var container = $("#blade-content-" + _bladeName);
        var form = require("hbs!templates/ticketTradForm")();

        container.empty();
        container.append(form);
    }

    function bindForm(translations) {

        if(translations) {
            if(translations.length > 0 && translations[0] && translations[0].length > 0 ) {
                $("#tickettradForm_name_fr").val(translations[0][0].text);
                $("#tickettradForm_name_en").val(translations[0][1].text);
                $("#tickettradForm_name_de").val(translations[0][2].text);
                $("#tickettradForm_name_nl").val(translations[0][3].text);
            }

            if(translations.length > 1 && translations[1] && translations[1].length > 0 ) {
                $("#tickettradForm_condition_fr").val(translations[1][0].text);
                $("#tickettradForm_condition_en").val(translations[1][1].text);
                $("#tickettradForm_condition_de").val(translations[1][2].text);
                $("#tickettradForm_condition_nl").val(translations[1][3].text);
            }
        }

        var inputs = $("#blade-content-" + _bladeName+ " input");
        inputs.on("change paste keyup", onUserInput);
    }

    function onUserInput() {

        _translations[0][0].text = $("#tickettradForm_name_fr").val();
        _translations[0][1].text = $("#tickettradForm_name_en").val();
        _translations[0][2].text = $("#tickettradForm_name_de").val();
        _translations[0][3].text = $("#tickettradForm_name_nl").val();

        _translations[1][0].text = $("#tickettradForm_condition_fr").val();
        _translations[1][1].text = $("#tickettradForm_condition_en").val();
        _translations[1][2].text = $("#tickettradForm_condition_de").val();
        _translations[1][3].text = $("#tickettradForm_condition_nl").val();

        _app.events.publish("ticketing_configTicket_trad-updated");
    }

});