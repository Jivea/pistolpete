/**
 * Created by Jivea
 * userHub.js
 * + Application module userHub.hbs
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var $ = require("jquery");
    var app = require("app");

    exports.init = function() {
        console.log("init");
        $("#menuHeader").on("click", function() {
            $("#userHub").toggleClass("opened");
        });

        $("#userHub").on("mouseleave", function() {
            $("#userHub").removeClass("opened");
        });

        $("#userHubMenu_SignOut").on("click", function() {
            $("#userHub").toggleClass("opened");
            app.navigate("signout");
        });
    };
});