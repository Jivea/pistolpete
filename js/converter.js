/**
 * Created by Jivea
 * converter.js
 * +
 */
/* global define */
define(function(require, exports, module) {
    "use strict";

    exports.cssTimeToMilliseconds =  function (timeString) {
        var num = parseFloat(timeString, 10);
        var unit = timeString.match(/m?s/);
        var milliseconds;

        if(unit) {
            unit = unit[0];
        }

        switch(unit) {
            case "s": // seconds
                milliseconds = num * 1000;
                break;
            case "ms": // milliseconds
                milliseconds = num;
                break;
            default:
                milliseconds = 0;
                break;
        }
        return milliseconds;
    }
});