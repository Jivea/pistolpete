/**
 * Created by Jivea
 * errorMessageExtractor.js
 * + module handling error message extraction out of an API error
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    exports.getMessage = function(error) {
        var message = "";
        if(typeof(error) == "object" && error.responseJSON != null) {
            if(error.responseJSON.ExceptionMessage != null) {
                message = error.responseJSON.ExceptionMessage;

            } else if(error.responseJSON.ModelState != null) {
                if(error.responseJSON.ModelState[""] != null) {
                    message = error.responseJSON.ModelState[""][0];

                } else {
                    message = error.responseJSON.ModelState[0];
                }
            } else if(error.responseJSON.Message != null) {
                message = error.responseJSON.Message;
            }
        }
        return message;
    };
});
