/**
 * Created by Jivea
 * handlebarsHelpers.js
 * +
 */

/* global define */
define(function(require, exports, module) {
    "use strict";

    var Handlebars = require("handlebars");
    var formatter = require("formatter");

    exports.registerHelpers = function() {
        Handlebars.registerHelper("formatDateToYYYYMMDD", formatter.formatDateToYYYYMMDD);
        Handlebars.registerHelper("formatMomentToLT", formatter.formatMomentToLT);
        Handlebars.registerHelper("formatMomentToFriendlyDate", formatter.formatMomentToFriendlyDate);

        Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        });
    }
});