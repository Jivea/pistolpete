console.log("Hello_1");

function Person(name, age) {
    this._name = name ;
    this._age = age;
}

Person.prototype.getName = function() {
  return this._name;
};
Person.prototype.getAge = function() {
    return this._age;
};
Person.prototype.introduce = function() {
  return "Name: "+this._name + " Age: "+ this._age;
};

function Employee(name, age, company) {
    Person.call(this, name, age);
    this._company = company;
}
Employee.prototype = Object.create(Person.prototype);
Employee.prototype.constructor = Person;
Employee.prototype.introduce = function() {
    return Person.prototype.introduce.call(this) + " Company:" + this._company;
};

console.log("Hello_2");

var dummyPerson = new Person("Johnny", 42);
console.log(dummyPerson.introduce());

console.log("Hello_3");
var employeePerson = new Employee("Toto", 33, "Barco");
console.log(employeePerson.introduce());

